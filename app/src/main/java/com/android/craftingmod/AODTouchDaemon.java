package com.android.craftingmod;

import android.os.HandlerThread;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import net.tarks.craftingmod.aodfix.api.Util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Scanner;

/**
 * Created by superuser on 16/11/13.
 */

public final class AODTouchDaemon {
    protected static int STOP = 7;
    protected static int CHECK = 12;

    private final HandlerThread thread;
    private final Handler handler;
    private final File inputToggle;
    private final File inputConsole;
    private final File consoleResult;
    public AODTouchDaemon(){
        inputToggle = new File("/sys/class/sec/tsp/input/enabled");
        inputConsole = new File("/sys/class/sec/tsp/cmd");
        consoleResult = new File("/sys/class/sec/tsp/cmd_result");

        thread = new HandlerThread("TangTangTang");
        thread.start();
        handler = new cHandler(thread.getLooper());
    }

    public void blockTouch(){
        handler.sendEmptyMessage(STOP);
    }
    private class cHandler extends Handler {
        private int code;
        private boolean toggleExist;
        public cHandler(Looper looper){
            super(looper);
            toggleExist = inputToggle.exists();
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == STOP){
                code = (int)Math.floor(Math.random()*5000);
                if(toggleExist){
                    // Note3 way
                    write(inputToggle,"0");
                }else{
                    // LTE-A way
                    write(inputConsole,"module_off_master," + code);
                    sendEmptyMessageDelayed(CHECK,300);
                }
            }else if(msg.what == CHECK){
                if(read(consoleResult).equalsIgnoreCase("module_off_master," + code + ":OK")){
                    Util.log("Modify OK.");
                }else{
                    sendEmptyMessage(STOP);
                }
            }
        }
        public String read(File file){
            try {
                return new Scanner(file).useDelimiter("\\Z").next();
            } catch (IOException e) {
                return null;
            }
        }
        public void write(File file,String content){
            Writer out;
            try {
                out = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file), "UTF-8"));
                out.write(content);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
