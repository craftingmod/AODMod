package net.tarks.craftingmod.aodfix.xposed.modules;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.Module;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by superuser on 16/6/18.
 */

public class TWTheme extends Module {
    public static String[] allowedPackages = new String[]{"com.samsung.android.themecenter","com.samsung.android.themestore"};
    private static final String APP_PACKAGE = "com.samsung.android.themestore";
    private static int THEMESTORE_VERSION = 0;
    private static String THEMESTORE_W_CLASS = null;
    private static String THEMESTORE_W_FIELD = null;
    private static String THEMESTORE_W_METHOD = null;
    @Override
    public void load() {
        if(packageName.equalsIgnoreCase("com.samsung.android.themecenter")){
            final String mName = "com.samsung.android.thememanager";
            final Class<?> themeManager = XposedHelpers.findClass(mName + ".ThemeManager",loader);
            final Class<?> themeDB = XposedHelpers.findClass(mName + ".ThemeDatabase",loader);

            XposedHelpers.findAndHookMethod(themeManager, "getSpecialEditionThemePackage", new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(null);
                    return null;
                }
            });
            XposedHelpers.findAndHookMethod(themeManager, "onMasterInstalled", String.class, boolean.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    Object _this = param.thisObject;
                    if(param.args[1] != null && param.args[2] != null){
                        Util.log("Theme installed by trial? : " + (param.args[1].equals(true)?"true":"false"));
                        Util.log("Theme installed where? : " + param.args[2]);
                    }else{
                        return;
                    }
                    param.args[1] = false;
                    // install_source_preloaded_theme vs install_source_theme_store vs install_source_side_loaded
                    // correct vs correct vs incorrect trial
                    if(param.args[2].equals("install_source_side_loaded")){
                        param.args[2] = "install_source_theme_store";
                    }
                }
            });

            try{
                XposedHelpers.findAndHookMethod(themeManager, "installAppIconPackage", Uri.class, boolean.class, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        Util.log("Icon trial : " + (param.args[1].equals(true)?"true":"false"));
                        param.args[1] = false;
                    }
                });
                //final Class<?> IThemeManager = XposedHelpers.findClass(mName + ".ThemeManagerService$5",loader);
            }catch (Exception e){
                e.printStackTrace();
            }

            //id hide
            XposedHelpers.findAndHookMethod(themeDB, "getUniqueDeviceId", Context.class, new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult("com.samsung.android.themecenter");
                    return "com.samsung.android.themecenter";
                }
            });
        }else if(packageName.equalsIgnoreCase("com.samsung.android.themestore")){
            XSharedPreferences pref = Util.getConfig();

            XposedHelpers.findAndHookMethod(JSONObject.class, "has", String.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    String key = (String) param.args[0];
                    if(key.equalsIgnoreCase("ad_visible")){
                        Util.log("sbs advertisement key? " + String.valueOf((boolean)param.getResult()));
                        param.setResult(false);
                    }
                }
            });
            Class<?> adpage = XposedHelpers.findClass("kr.co.sbs.adplayer.pages.PlayerPage",loader);
            XposedHelpers.findAndHookMethod(adpage, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    XposedBridge.log("ADPlayer - call finish");
                    Activity act = (Activity)param.thisObject;
                    Intent i = new Intent();
                    i.putExtra("result",-12);
                    i.putExtra("uuid","Hooked");
                    i.putExtra("tid","Metapod");
                    i.putExtra("id","ADBlock");
                    i.putExtra("position",Integer.MAX_VALUE);
                    act.setResult(0,i);
                    act.finish();
                }
            });
            Class<?> screenshotA = XposedHelpers.findClass("com.samsung.android.themestore.activity.ScreenShotActivity",loader);
            XposedHelpers.findAndHookMethod(screenshotA, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Activity act = (Activity) param.thisObject;
                    Intent i = act.getIntent();
                    if(i != null){
                        if(i.hasExtra("EXTRA_KEY_SCREENSHOT_URLS")){
                            ArrayList<String> list = i.getStringArrayListExtra("EXTRA_KEY_SCREENSHOT_URLS");
                            StringBuilder out = new StringBuilder();
                            String ls = "\n";
                            for(String s : list) {
                                out.append(s).append(ls);
                            }
                            Util.log("screenshot urls:\n" + out.toString());
                        }
                    }
                }
            });
            XposedHelpers.findAndHookMethod(Intent.class, "putExtra", String.class, String.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    if(param.args[0].equals("downloadUri")){
                        Util.log("downloadURL: " + param.args[1]);
                    }
                }
            });
            /**
             * Wallpaper trial enabler script
             * Obfused theme store
             */
            // root value
            String hookClsName = null;
            char[] alphabets = "abcdefghijklmnopqrstuvwxyz".toCharArray();
            // First. get version code
            final Object activityThread = XposedHelpers.callStaticMethod(XposedHelpers.findClass("android.app.ActivityThread", null), "currentActivityThread");
            final Context context = (Context) XposedHelpers.callMethod(activityThread, "getSystemContext");
            int versionCode;
            try {
                versionCode = context.getPackageManager().getPackageInfo(APP_PACKAGE, 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                versionCode = -1;
            }

            // Second. compare version code
            if(THEMESTORE_VERSION == versionCode){
                // load used class name
                hookClsName = THEMESTORE_W_CLASS;
            }
            // Third. find class if no saved.
            if(hookClsName == null){
                // search class
                String frag_prefix = "com.samsung.android.themestore.activity.fragment.";
                int i = 0;
                Class<?> find;
                while(true){
                    // com.samsung.android.themestore.activity.fragment.[a-z][a-z]
                    find = XposedHelpers.findClassIfExists(
                            new StringBuilder().append(frag_prefix)
                                    .append(alphabets[(int) Math.floor(i/alphabets.length)]).append(alphabets[i%alphabets.length]).toString(),loader);
                    if(find == null){
                        // end of classes.
                        break;
                    }
                    boolean valid = false;
                    // find String field and check it is "a" name
                    try{
                        // check implements onClickListener
                        if(View.OnClickListener.class.isAssignableFrom(find)){
                            // check has "a" string field
                            Field f = XposedHelpers.findFieldIfExists(find,"a");
                            if(f != null && String.class.isAssignableFrom(f.getType()) && Modifier.isStatic(f.getModifiers())){
                                // valid!
                                hookClsName = find.getName();
                                valid = true;
                            }
                        }
                    }catch (NoSuchFieldError e){
                    }
                    if(valid){
                        // found class
                        break;
                    }
                    i += 1;
                }
            }
            // If found class
            if(hookClsName != null){
                Class<?> storeDetailCls = XposedHelpers.findClassIfExists(hookClsName,loader);
                if(storeDetailCls != null){
                    Util.log("Wallpaper hooking class: " + hookClsName);
                    THEMESTORE_VERSION = versionCode;
                    THEMESTORE_W_CLASS = hookClsName;
                    // 4. get first int field
                    try{
                        Field mField = null;
                        if(THEMESTORE_W_FIELD != null){
                            // using cache
                            mField = XposedHelpers.findFieldIfExists(storeDetailCls,THEMESTORE_W_FIELD);
                        }
                        if(mField == null){
                            // find field
                            for(int i=0;i<alphabets.length;i+=1){
                                mField = XposedHelpers.findFieldIfExists(storeDetailCls,String.valueOf(alphabets[i]));
                                if(mField != null && int.class.isAssignableFrom(mField.getType())){
                                    boolean con = false;
                                    try {
                                        storeDetailCls.getDeclaredField(mField.getName());
                                    } catch (NoSuchFieldException e) {
                                        con = true;
                                    }
                                    if(con){
                                        mField = null;
                                        continue;
                                    }else{
                                        break;
                                    }
                                }
                                mField = null;
                            }
                        }
                        if(mField != null){
                            final String typeName = mField.getName();
                            THEMESTORE_W_FIELD = typeName;
                            // 5. find method
                            String methodN = null;
                            if(THEMESTORE_W_METHOD != null){
                                methodN = THEMESTORE_W_METHOD;
                            }else{
                                char[] searches = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
                                Method[] mds = storeDetailCls.getDeclaredMethods();
                                HashMap<String,Method> maps = new HashMap<>();
                                for (Method m : mds) {
                                    maps.put(m.getName(), m);
                                }
                                // align method..
                                boolean findBR = false; // find boolean return
                                for(int i=0;i<searches.length;i+=1){
                                    String p = String.valueOf(searches[i]);
                                    if(maps.containsKey(p) && maps.get(p).getParameterTypes().length == 0){
                                        Method m = maps.get(p);
                                        // find simple method
                                        if(findBR){
                                            methodN = p;
                                            break;
                                        }else{
                                            if(Boolean.TYPE.equals(m.getReturnType())){
                                                findBR = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if(methodN != null){
                                final String methodName = methodN;
                                XposedHelpers.findAndHookMethod(storeDetailCls, methodN, new XC_MethodHook() {
                                    private int original = 1;
                                    @Override
                                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                                        Util.log("Wallpaper method " + methodName + " field " + typeName + " changed -> " + XposedHelpers.getIntField(param.thisObject,typeName));
                                        original = XposedHelpers.getIntField(param.thisObject,typeName);
                                        XposedHelpers.setIntField(param.thisObject,typeName,2);
                                    }

                                    @Override
                                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                                        XposedHelpers.setIntField(param.thisObject,typeName,original);
                                    }
                                });
                            }else{
                                Util.log("Wallpaper:can't find method");
                            }
                            // 5. hook field! (If fragment)
                            /*
                            XposedHelpers.findAndHookMethod(storeDetailCls, "onCreate", Bundle.class, new XC_MethodHook() {
                                @Override
                                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                                    Util.log("Wallpaper field " + typeName + " changed -> " + XposedHelpers.getIntField(param.thisObject,typeName));
                                    XposedHelpers.setIntField(param.thisObject,typeName,3);
                                }
                            });
                            */
                        }
                    }catch (NoSuchFieldError | NoSuchMethodError e){
                        Util.log("Wrong class? " + hookClsName);
                    }
                }else{
                    Util.log("Cls name " + hookClsName + " not found!");
                }
            }else {
                Util.log("Wallpaper hooking not found!");
            }

        }
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.samsung.android.themecenter") || packageName.equalsIgnoreCase("com.samsung.android.themestore");
    }

    protected TWTheme(XC_LoadPackage.LoadPackageParam pm) {
        super(pm);
    }
    public static TWTheme newInstance(XC_LoadPackage.LoadPackageParam pm){
        return new TWTheme(pm);
    }

}
