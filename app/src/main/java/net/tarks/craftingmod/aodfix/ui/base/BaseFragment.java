package net.tarks.craftingmod.aodfix.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.IBController;
import net.tarks.craftingmod.aodfix.ui.ISaver;

/**
 * Created by superuser on 16/10/2.
 */

public abstract class BaseFragment extends Fragment {
    protected ISaver mSaver;
    protected IBController mBrightCon;
    protected Context context;
    @Override
    public void onAttach(Context cn) {
        super.onAttach(cn);
        context = cn;
        if(context instanceof ISaver){
            mSaver = (ISaver) context;
            Util.log("ISaver ready");
        }
        if(context instanceof IBController){
            mBrightCon = (IBController) context;
        }
    }
}
