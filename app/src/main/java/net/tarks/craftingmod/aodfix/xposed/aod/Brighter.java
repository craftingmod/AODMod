package net.tarks.craftingmod.aodfix.xposed.aod;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import de.robv.android.xposed.XposedHelpers;

/**
 * Created by superuser on 16/6/18.
 */

public class Brighter implements SensorEventListener,Runnable,Animation.AnimationListener {

    private final int delay = 1500;

    private final Object AODService;
    private View clockView;
    private Handler handler;
    private int i;
    private int count;
    public static final float[] lights = new float[]{0.01f,   10,   20,   40,   70,  170,  240,  470,660};
    public static final float[] alphas = new float[]{0.40f,0.50f,0.60f,0.70f,0.80f,0.85f,0.90f,0.95f,1f ,1f};
    private final int totalL;
    private final float oneLv;
    private final float minLv = 0.2F;

    private float progressA;

    private float nowLv;
    private float appliedLv;


    public Brighter(Object service){
        nowLv = minLv;
        progressA = 1F;
        count = 0;
        appliedLv = 1F;
        clockView = null;
        // 1 ~ 7

        totalL = lights.length;
        oneLv = (1F-minLv) / (totalL-1);

        AODService = service;
    }
    public void reset(Handler h){
        count = 0;
        nowLv = 1F;
        progressA = 1F;
        appliedLv = 1F;
        clockView = null;
        handler = h;
    }
    public void destroy(Handler han){
        try{
            han.removeCallbacks(this);
        }catch (Exception e){
        }finally {
            try{
                SensorManager sensorManager = (SensorManager) XposedHelpers.getObjectField(AODService,"mSensorManager");
                Sensor sensorL = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
                sensorManager.unregisterListener(this,sensorL);
            }catch (Exception e){
            }
        }
    }
    public void destroy(){
        destroy(handler);
    }
    public void reapply(){
        reapply(null);
    }
    public void reapply(Object clock){
        if(clockView == null && clock != null){
            clockView = (View)clock;
        }
        if(clockView == null){
            return;
        }
        if(clockView.getAnimation() != null && !clockView.getAnimation().hasEnded()){
            clockView.getAnimation().cancel();
        }
        final AlphaAnimation anim = new AlphaAnimation(nowLv, nowLv);
        anim.setDuration(0);
        anim.setStartOffset(0);
        anim.setFillAfter(true);
        clockView.startAnimation(anim);
        appliedLv = nowLv;
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_LIGHT){
            float bright = event.values[0];
            for(i=0;i<totalL;i+=1){
                if(bright < lights[i]){
                    nowLv = alphas[i];
                    break;
                }
                if(i == totalL-1){
                    nowLv = alphas[i+1];
                }
            }
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void run() {
        Object clock = null;
        try{
            clock = XposedHelpers.getObjectField(XposedHelpers.getObjectField(AODService,"mAODBaseClockView"),"mRootView");
        }catch (NoSuchFieldError | NullPointerException e){
            e.printStackTrace();
            return;
        }
        if(clock != null && clockView == null){
            clockView = (View)clock;
        }else if(clock == null && clockView != null){
            clockView = null;
        }
        if(clock == null){
            count += 1;
        }else{
            count = 0;
        }
        if(appliedLv != nowLv && clockView != null){
            // should apply level
            if(clockView.getAnimation() != null && !clockView.getAnimation().hasEnded()){
            }else{
                progressA = nowLv;
                final AlphaAnimation anim = new AlphaAnimation(appliedLv, nowLv);
                anim.setDuration(1000);
                anim.setStartOffset(0);
                anim.setFillAfter(true);
                anim.setAnimationListener(this);
                clockView.startAnimation(anim);
            }
        }

        if(count < 5){
            handler.postDelayed(this,delay);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {}

    @Override
    public void onAnimationEnd(Animation animation) {
        appliedLv = progressA;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}
