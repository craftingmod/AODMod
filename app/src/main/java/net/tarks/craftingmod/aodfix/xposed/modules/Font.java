package net.tarks.craftingmod.aodfix.xposed.modules;

import net.tarks.craftingmod.aodfix.xposed.Module;

import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static net.tarks.craftingmod.aodfix.api.Util.log;

/**
 * Created by superuser on 16/8/3.
 */

public class Font extends Module {
    public static String[] allowedPackages = new String[]{"com.android.settings"};

    protected Font(XC_LoadPackage.LoadPackageParam pm) {
        super(pm);
    }

    @Override
    public void load() {
        Class<?> FontPreview = XposedHelpers.findClass("com.android.settings.FontPreview",loader);
        XposedHelpers.findAndHookMethod(FontPreview, "checkFont", String.class, new XC_MethodReplacement() {
            @Override
            protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                log("checkFont! -> return false hooked.");
                param.setResult(false);
                return false;
            }
        });
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.android.settings");
    }
    public static Font newInstance(XC_LoadPackage.LoadPackageParam pm){
        return new Font(pm);
    }
}
