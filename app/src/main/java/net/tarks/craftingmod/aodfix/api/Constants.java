package net.tarks.craftingmod.aodfix.api;

import android.graphics.Color;

/**
 * Created by superuser on 16/9/30.
 */

public final class Constants {
    public static final String PREF_AOD_DURATION_WIFI = "aod_duration_wifi";
    public static final String PREF_AOD_DURATION_WIFI_INFINITY = "aod_duration_wifi_infinity";
    public static final String PREF_AOD_DURATION_MOBILE = "aod_duration_mobile";
    public static final String PREF_AOD_DURATION_MOBILE_INFINITY = "aod_duration_mobile_infinity";

    public static final String PREF_AOD_BRIGHTNESS_AUTO = "aod_brightness_useauto";
    public static final String PREF_AOD_BRIGHTNESS_VALUE = "aod_brightness_light";

    public static final String PREF_AOD_NOTIFICATION_NUMBER = "aod_noti_num";
    public static final String PREF_AOD_NOTIFICATION_COLOR = "aod_noti_color";

    public static final String PREF_AOD_BATTERY_SAFETY = "aod_battery_safety";
    public static final String PREF_AOD_SCREENSHOT = "screenshot_Aod";
    public static final String PREF_AOD_NATIVE_WAKELOCK = "aod_wakelock_internal";

    public static final String PREF_LAST_UPDATE_CHECK = "checkupdate_last";

    public static final String PREF_AOD_KNOCK_ON = "knockon";

    public static final String PREF_DEBUG = "debug";

    // should change
    // -6690917
    /*
    public static int COLOR_BATTERY_FILL = Color.parseColor("#99e79b");
    public static int COLOR_BATTERY_FILL_DARK = Color.parseColor("#69ba6b");
    public static int COLOR_BATTERY_BACK = Color.parseColor("#e04e664f");
    public static int COLOR_BATTERY_BACK_DARK = Color.parseColor("#89436e43");
    public static int COLOR_OUTLINE_WHITE = Color.parseColor("#ffffff");
    public static int COLOR_OUTLINE_BLACK = Color.parseColor("#555555");
    */
    public static int COLOR_BATTERY_FILL = Color.parseColor("#000000");
    public static int COLOR_BATTERY_FILL_DARK = Color.parseColor("#000000");
    public static int COLOR_BATTERY_BACK = Color.parseColor("#000000");
    public static int COLOR_BATTERY_BACK_DARK = Color.parseColor("#000000");
    public static int COLOR_OUTLINE_WHITE = Color.parseColor("#000000");
    public static int COLOR_OUTLINE_BLACK = Color.parseColor("#000000");
    public static int COLOR_BOLT_WHITE = Color.parseColor("#000000");
    public static int COLOR_BOLT_BLACK = Color.parseColor("#000000");

}
