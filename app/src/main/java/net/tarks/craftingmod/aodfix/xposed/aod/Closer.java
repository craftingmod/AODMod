package net.tarks.craftingmod.aodfix.xposed.aod;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import net.tarks.craftingmod.aodfix.api.Util;

import de.robv.android.xposed.XposedHelpers;

/**
 * Created by superuser on 16/11/12.
 */

public class Closer implements Runnable {
    private final Object AODService;
    private Handler handler;
    public Closer(Object aodService){
        AODService = aodService;
    }
    public void reset(Handler handle){
        handler = handle;
    }
    @Override
    public void run() {
        exit();
    }
    public void destroy(){
        try{
            handler.removeCallbacks(this);
        }catch (Exception e){}
    }
    private void exit(){
        Util.log("Closing AOD..");
        Context c = (Context) AODService;
        Intent i = new Intent();
        i.setAction("tomorrow.aod.touch.lock");
        c.sendBroadcast(i);

        Handler h = (Handler) XposedHelpers.getObjectField(AODService,"mUIHandler");
        h.sendEmptyMessage(XposedHelpers.getIntField(AODService,"UIHANDLER_FORCE_STOP_ALPM"));
        handler.post(new Runnable() {
            @Override
            public void run() {
                XposedHelpers.callMethod(AODService,"aodScreenTurnedOn");
            }
        });
    }
}
