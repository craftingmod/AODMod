package net.tarks.craftingmod.aodfix.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kyleduo.switchbutton.SwitchButton;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.base.DarkFragment;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import static net.tarks.craftingmod.aodfix.xposed.aod.Brighter.alphas;
import static net.tarks.craftingmod.aodfix.xposed.aod.Brighter.lights;

/**
 * Created by superuser on 16/10/2.
 */

public class LightFragment extends DarkFragment implements SensorEventListener {
    private View rootView;

    private int bright;
    private boolean auto;
    private DiscreteSeekBar seekbar;
    private SwitchButton switchAuto;
    private SharedPreferences pref;

    private SensorManager sensorM;

    private LinearLayout topLayout;
    private LinearLayout bottomLayout;

    public LightFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.bright_frag, container, false);
        pref = Util.getConfig(context);

        bright = pref.getInt(Constants.PREF_AOD_BRIGHTNESS_VALUE,20);
        auto = pref.getBoolean(Constants.PREF_AOD_BRIGHTNESS_AUTO,false);

        topLayout = (LinearLayout) rootView.findViewById(R.id.topL);
        bottomLayout = (LinearLayout) rootView.findViewById(R.id.bottomL);
        seekbar = (DiscreteSeekBar) rootView.findViewById(R.id.customBright);
        switchAuto = (SwitchButton) rootView.findViewById(R.id.switchAuto);
        seekbar.setProgress(bright);
        switchAuto.setCheckedNoEvent(auto);

        initCreateView();

        switchAuto.setOnCheckedChangeListener((compoundButton, b) -> {
            auto = b;
            updateValue(true);
        });
        seekbar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                if(value != bright){
                    bright = value;
                    updateValue(true);
                }
            }
            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {}
        });
        return rootView;
    }

    @Override
    protected void onFirstInit() {
        super.onFirstInit();
        initBright(context);
    }

    @Override
    protected void onSwitchedFragment(boolean visible) {
        super.onSwitchedFragment(visible);
        if(visible){
            initBright(context);
        }else{
            if(sensorM != null){
                sensorM.unregisterListener(this);
            }
            mBrightCon.setActionBarOrigin();
            mBrightCon.recoverBright();
        }
    }

    private void updateValue(boolean save){
        if(auto){
            seekbar.setEnabled(false);
            mBrightCon.setBrightness(100);
        }else{
            topLayout.setAlpha(1.0F);
            bottomLayout.setAlpha(1.0F);
            seekbar.setEnabled(true);
            mBrightCon.setBrightness(bright);
        }
        if(save){
            mSaver.request(Constants.PREF_AOD_BRIGHTNESS_AUTO,auto);
            mSaver.request(Constants.PREF_AOD_BRIGHTNESS_VALUE,bright);
            mSaver.updateSave();
        }
    }
    public static LightFragment newInstance() {
        LightFragment fragment = new LightFragment();
        return fragment;
    }
    public void initBright(Context con){
        sensorM = (SensorManager) con.getSystemService(Context.SENSOR_SERVICE);
        sensorM.registerListener(this,sensorM.getDefaultSensor(Sensor.TYPE_LIGHT),SensorManager.SENSOR_DELAY_NORMAL);
        updateValue(false);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(auto){
            if(event.sensor.getType() == Sensor.TYPE_LIGHT){
                float bright = event.values[0];
                float alpha = 1.0F;
                int totalL = lights.length;
                for(int i=0;i<totalL;i+=1){
                    if(bright < lights[i]){
                        alpha = alphas[i];
                        break;
                    }
                    if(i == totalL-1){
                        alpha = alphas[i+1];
                    }
                }
                topLayout.setAlpha(alpha);
                bottomLayout.setAlpha(alpha);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}
}
