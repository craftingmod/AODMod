package net.tarks.craftingmod.aodfix.ui;

/**
 * Created by superuser on 16/10/2.
 */

public interface IBController {
    void setBrightness(int bright);
    void recoverBright();
    void setActionBarColor(int back,int title,int status);
    void setActionBarOrigin();
}
