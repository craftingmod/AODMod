package net.tarks.craftingmod.aodfix.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.mikepenz.crossfader.util.UIUtils;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.base.BaseFragment;

import java.lang.reflect.Field;

import io.feeeei.circleseekbar.CircleSeekBar;


public class TimePickerFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "toEdit";

    // TODO: Rename and change types of parameters
    private String toEdit;


    private View view;
    private CircleSeekBar outerBar;
    private CircleSeekBar innerBar;

    private TextView timeText;
    private Switch switchInf;

    private int hour;
    private int minutes;
    private boolean infinity;

    private String saveTo;

    private SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(toEdit == null){
            return null;
        }
        view = inflater.inflate(R.layout.timepicker_frag, container, false);
        pref = Util.getConfig(context);

        int total = Math.max(Math.min(86399,pref.getInt(toEdit,900)),1);
        hour = (int) Math.floor(total/3600);
        minutes = (int) Math.floor((total%3600)/60);
        infinity = pref.getBoolean(toEdit + "_infinity",true);

        outerBar = (CircleSeekBar) view.findViewById(R.id.outerSeek);
        innerBar = (CircleSeekBar) view.findViewById(R.id.innerSeek);
        Util.fixCircleSeekbar(outerBar, (int)UIUtils.convertDpToPixel(300,context));
        Util.fixCircleSeekbar(innerBar, (int)UIUtils.convertDpToPixel(236,context));
        outerBar.setCurProcess(minutes);
        innerBar.setCurProcess(hour);
        outerBar.setOnSeekBarChangeListener((circleSeekBar, i) -> {
            if(minutes != i){
                minutes = i;
                updateTime(true);
            }
        });
        innerBar.setOnSeekBarChangeListener((circleSeekBar, i) -> {
            if(hour != i){
                hour = i;
                updateTime(true);
            }
        });

        timeText = (TextView) view.findViewById(R.id.timeText);
        switchInf = (Switch) view.findViewById(R.id.switch_infinity);
        switchInf.setChecked(infinity);
        switchInf.setOnCheckedChangeListener((compoundButton, b) -> {
            if(infinity != b){
                infinity = b;
                updateTime(true);
            }
        });

        updateTime(false);

        return view;
    }
    private String timeFormat_H;
    private String timeFormat_M;
    private void updateTime(boolean save){
        if(save && mSaver != null){
            int total = Math.max(Math.min(86399,3600*hour+60*minutes),1);
            mSaver.request(toEdit,total);
            mSaver.request(toEdit + "_infinity",infinity);
            mSaver.updateSave();
        }
        if(infinity){
            timeText.setVisibility(View.GONE);
        }else{
            timeText.setVisibility(View.VISIBLE);
        }
        // toggle progressing
        try {
            Field field = CircleSeekBar.class.getDeclaredField("isCanTouch");
            field.setAccessible(true);
            field.setBoolean(outerBar,!infinity);
            field.setBoolean(innerBar,!infinity);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if(timeFormat_H == null || timeFormat_M == null){
            timeFormat_H = context.getString(R.string.timeFormat_H);
            timeFormat_M = context.getString(R.string.timeFormat_M);
        }
        String hourStr = hour + "";
        String minuteStr = minutes > 9 ? minutes + "" : "0" + minutes;
        if(hour == 0){
            timeText.setText(timeFormat_M.replace("%p",minutes+""));
        }else if(minutes == 0){
            timeText.setText(timeFormat_H.replace("%p",hourStr));
        }else{
            timeText.setText(timeFormat_H.replace("%p",hourStr) + " " + timeFormat_M.replace("%p",minuteStr));
        }
    }

    public TimePickerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param spEdit what's to write?
     * @return A new instance of fragment TimePickerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimePickerFragment newInstance(String spEdit) {
        TimePickerFragment fragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, spEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            toEdit = getArguments().getString(ARG_PARAM1);
        }
    }
}
