package net.tarks.craftingmod.aodfix.xposed;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;

import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by superuser on 16/6/18.
 */

public abstract class Module {
    public static String[] allowedPackages = {};

    protected final ClassLoader loader;
    protected final ApplicationInfo appInfo;
    protected final String packageName;
    protected final boolean hasConfig;
    protected final XSharedPreferences config;
    protected Module(XC_LoadPackage.LoadPackageParam pm){
        this(pm,null);
    }
    protected Module(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences conf){
        this.loader = pm.classLoader;
        this.appInfo = pm.appInfo;
        this.packageName = pm.packageName;
        if(conf != null){
            this.config = conf;
            this.hasConfig = true;
        }else{
            this.config = null;
            this.hasConfig = false;
        }
    }
    protected int getResourceId(Context context, String pVariableName, String pResourcename, String pPackageName) {
        return getResourceId(context.getResources(),pVariableName,pResourcename,pPackageName);
    }
    protected int getResourceId(Resources res, String pVariableName, String pResourcename, String pPackageName) {
        try {
            return res.getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    public abstract void load();
    public abstract boolean filter();
}
