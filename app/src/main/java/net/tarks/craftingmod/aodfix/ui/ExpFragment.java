package net.tarks.craftingmod.aodfix.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyleduo.switchbutton.SwitchButton;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.base.DarkFragment;

import java.util.ArrayList;

import eu.chainfire.libsuperuser.Shell;

/**
 * Created by superuser on 16/10/3.
 */

public class ExpFragment extends DarkFragment {
    private View view;
    private SwitchButton screenshot;
    private boolean su;
    public ExpFragment(){

    }
    public static ExpFragment newInstance(){
        return new ExpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.exp_frag, container, false);
        SharedPreferences pref = Util.getConfig(context);

        ArrayList<SwitchButton> buttons = getButtons(view);
        for(int i=0;i<buttons.size();i+=1){
            SwitchButton btn = buttons.get(i);
            if(btn.getTag() != null && !btn.getTag().equals("")){
                btn.setCheckedNoEvent(pref.getBoolean((String) btn.getTag(),false));
                btn.setOnCheckedChangeListener((compoundButton, b) -> {
                    String code = (String) compoundButton.getTag();
                    mSaver.request(code,b);
                    mSaver.updateSave();
                });
            }
        }
        screenshot = (SwitchButton) view.findViewById(R.id.toggleShot);

        this.initCreateView();
        return view;
    }

    @Override
    protected void onFirstInit() {
        super.onFirstInit();
        su = Shell.SU.available();
        updateScreenSwitch();
    }

    private void updateScreenSwitch(){
        screenshot.setCheckedNoEvent(Settings.System.getInt(context.getContentResolver(), "aod_screen_capture_mode", 0) == 1);
        if(su){
            screenshot.setEnabled(true);
            screenshot.setOnCheckedChangeListener((buttonView, isChecked) -> {
                screenshot.setEnabled(false);
                new Thread(() -> {
                    Shell.SU.run("settings put system aod_screen_capture_mode " + (isChecked?"1":"0"));
                }).start();
                new android.os.Handler(context.getMainLooper()).postDelayed(() -> updateScreenSwitch(),2000);
            });
        }else{
            screenshot.setOnCheckedChangeListener(null);
            screenshot.setEnabled(false);
        }
    }

    public ArrayList<SwitchButton> getButtons(View rootV) {
        ArrayList<SwitchButton> buttons = new ArrayList<>();
        ViewGroup viewGroup = (ViewGroup) rootV;
        findButtons(viewGroup, buttons);
        return buttons;
    }

    private static void findButtons(ViewGroup viewGroup,ArrayList<SwitchButton> buttons) {
        for (int i = 0, N = viewGroup.getChildCount(); i < N; i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof ViewGroup) {
                findButtons((ViewGroup) child, buttons);
            } else if (child instanceof SwitchButton) {
                buttons.add((SwitchButton) child);
            }
        }
    }
}
