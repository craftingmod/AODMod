package net.tarks.craftingmod.aodfix.xposed.modules;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.mikepenz.crossfader.util.UIUtils;

import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.xposed.Module;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static net.tarks.craftingmod.aodfix.api.Util.log;

/**
 * Created by superuser on 16/10/15.
 */

public class SafetyBattery extends Module {
    public static String[] allowedPackages = new String[]{"com.samsung.android.app.aodservice","com.android.systemui"};
    private boolean use;
    private boolean dropV;
    private boolean goodLock;

    protected SafetyBattery(XC_LoadPackage.LoadPackageParam pm, XSharedPreferences conf) {
        super(pm, conf);
        use = config.getBoolean(Constants.PREF_AOD_BATTERY_SAFETY,false);
        dropV = config.getBoolean("drop_greenV",false);
    }
    public static SafetyBattery newInstance(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences pf){
        return new SafetyBattery(pm,pf);
    }

    @Override
    public void load() {
        if(packageName.equalsIgnoreCase("com.samsung.android.app.aodservice")){
            final Class<?> AODBattery = XposedHelpers.findClassIfExists("com.samsung.android.app.aodservice.utils.BatteryMeterView",loader);
            if(AODBattery != null){
                XposedHelpers.findAndHookMethod(AODBattery, "prepareFramePaint", Resources.class, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        try{
                            boolean useGreen = use;
                            boolean useV = !dropV;
                            log("Use green: " + (useGreen?"true":"false"));
                            if(useGreen){
                                XposedHelpers.setBooleanField(param.thisObject,"mUseGreenBatteryIcon", true);
                                XposedHelpers.setBooleanField(param.thisObject,"mShowBatterySafeIcon", useV);
                                int[] levels = (int[]) XposedHelpers.getObjectField(param.thisObject,"GREEN_LEVELS");
                                int[] colors = (int[]) XposedHelpers.getObjectField(param.thisObject,"GREEN_COLORS");
                                XposedHelpers.setObjectField(param.thisObject,"mGlobalLevels", levels);
                                XposedHelpers.setObjectField(param.thisObject,"mGlobalColors", colors);
                                // white
                                colors[2] = Constants.COLOR_BATTERY_FILL;

                                ArrayList<Integer> colorOut = new ArrayList<>();
                                for(int i=0;i<levels.length;i+=1){
                                    colorOut.add(levels[i]);
                                    colorOut.add(colors[i]);
                                }
                                int[] output = convertIntegers(colorOut);
                                // output = colorOut.stream().mapToInt(i -> i).toArray();
                                XposedHelpers.setObjectField(param.thisObject,"mColors",output);
                                log("SetObject - mColors");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }else if(packageName.equalsIgnoreCase("com.android.systemui")){
            hookSystemUI();
        }
    }
    private void hookSystemUI(){
        final String pkg = packageName;
        final Class<?> BatteryView = XposedHelpers.findClass("com.android.systemui.BatteryMeterView",loader);
        final Class<?> Feature_TW = XposedHelpers.findClassIfExists("com.android.systemui.statusbar.Feature",loader);
        final Class<?> Feature_GoodLock = XposedHelpers.findClassIfExists("com.android.systemui.opensesame.core.Features",loader);
        //XposedHelpers.setStaticBooleanField(Feature,"mUsePureSignalCluster",true);
        goodLock = Feature_GoodLock != null;
        final float size = 0.8f;

        XposedHelpers.findAndHookConstructor(BatteryView, Context.class, AttributeSet.class, int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                Context context = (Context) param.args[0];
                XposedHelpers.setAdditionalInstanceField(param.thisObject,"oContext",context);

                Resources res = context.getResources();
                ArrayList<Integer> colors = new ArrayList<>();
                TypedArray array_lv = res.obtainTypedArray(getResourceId(res,"batterymeter_color_levels","array",pkg));
                TypedArray array_color = res.obtainTypedArray(getResourceId(res,"batterymeter_color_values","array",pkg));
                for(int i=0;i<array_lv.length();i+=1){
                    colors.add(array_lv.getInt(i,0));
                    colors.add(array_color.getColor(i,0));
                }
                XposedHelpers.setObjectField(param.thisObject,"mColors",convertIntegers(colors));
                XposedHelpers.setIntField(param.thisObject,"mLightModeFillColor", Constants.COLOR_BATTERY_FILL);
                XposedHelpers.setIntField(param.thisObject,"mDarkModeFillColor",Constants.COLOR_BATTERY_FILL_DARK);
                XposedHelpers.setIntField(param.thisObject,"mLightModeBackgroundColor", ContextCompat.getColor(context,
                        getResourceId(res,"light_mode_battery_icon_color_dual_tone_background","color",pkg)));
                XposedHelpers.setIntField(param.thisObject,"mDarkModeBackgroundColor", ContextCompat.getColor(context,
                        getResourceId(res,"dark_mode_battery_icon_color_dual_tone_background","color",pkg)));
                XposedHelpers.setIntField(param.thisObject,"mLightModeBoltColor", ContextCompat.getColor(context,
                        getResourceId(res,"batterymeter_bolt_color","color",pkg)));
                XposedHelpers.setIntField(param.thisObject,"mDarkModeBoltColor", ContextCompat.getColor(context,
                        getResourceId(res,"batterymeter_bolt_color_light","color",pkg)));

                XposedHelpers.setAdditionalInstanceField(param.thisObject,"mDarkMode",false);
                XposedHelpers.setAdditionalInstanceField(param.thisObject,"mOutlineSize", UIUtils.convertDpToPixel(size,(Context) param.args[0]));
                XposedHelpers.setAdditionalInstanceField(param.thisObject,"mDarkEdge",Constants.COLOR_OUTLINE_BLACK);
                XposedHelpers.setAdditionalInstanceField(param.thisObject,"mLightEdge",Constants.COLOR_OUTLINE_WHITE);

                log("Applied greeeen battery");
            }
        });
        XposedHelpers.findAndHookMethod(BatteryView, "getFillColor", float.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                int result = (int)param.getResult();
                if(result == XposedHelpers.getIntField(param.thisObject,"mLightModeFillColor")){
                    XposedHelpers.setAdditionalInstanceField(param.thisObject,"mDarkMode",false);
                }else if(result == XposedHelpers.getIntField(param.thisObject,"mDarkModeFillColor")){
                    XposedHelpers.setAdditionalInstanceField(param.thisObject,"mDarkMode",true);
                }
            }
        });
            /*
            XposedHelpers.findAndHookMethod(BatteryView, "getFillColor", float.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    int result = (int)param.getResult();
                    if(result == XposedHelpers.getIntField(param.thisObject,"mLightModeFillColor")){
                        param.setResult(Constants.COLOR_BATTERY_FILL);
                    }else if(result == XposedHelpers.getIntField(param.thisObject,"mDarkModeFillColor")){
                        param.setResult(Constants.COLOR_BATTERY_FILL_DARK);
                    }
                }
            });
            */
            XposedHelpers.findAndHookMethod(BatteryView, "getBackgroundColor", float.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    int result = (int)param.getResult();
                    if(result == XposedHelpers.getIntField(param.thisObject,"mLightModeBackgroundColor")){
                        param.setResult(Constants.COLOR_BATTERY_BACK);
                    }else if(result == XposedHelpers.getIntField(param.thisObject,"mDarkModeBackgroundColor")){
                        param.setResult(Constants.COLOR_BATTERY_BACK_DARK);
                    }
                }
            });
        XposedHelpers.findAndHookMethod(BatteryView, "getBoltColor", float.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                int colorReturn = (int) param.getResult();
                if(colorReturn == XposedHelpers.getIntField(param.thisObject,"mDarkModeBoltColor")){
                    param.setResult(Constants.COLOR_BOLT_BLACK);
                }else if(colorReturn == XposedHelpers.getIntField(param.thisObject,"mLightModeBoltColor")){
                    param.setResult(Constants.COLOR_BOLT_WHITE);
                }
            }
        });
        XposedHelpers.findAndHookMethod(BatteryView, "draw", Canvas.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                Object _this = param.thisObject;

                int width = XposedHelpers.getIntField(_this,"mWidth");
                int height = XposedHelpers.getIntField(_this,"mHeight");
                Paint outlinePaint;
                Path batteryFrame;
                if(XposedHelpers.getAdditionalInstanceField(_this,"mOutlinePaint") != null
                        && (int)XposedHelpers.getAdditionalInstanceField(_this,"lastWidth") == width
                        && (int)XposedHelpers.getAdditionalInstanceField(_this,"lastHeight") == height){
                    outlinePaint = (Paint) XposedHelpers.getAdditionalInstanceField(_this,"mOutlinePaint");
                    batteryFrame = (Path) XposedHelpers.getAdditionalInstanceField(_this,"mOutlineFrame");
                }else{
                    log("redrawing..");
                    // paint set
                    float outlineSize = (float) XposedHelpers.getAdditionalInstanceField(_this,"mOutlineSize");
                    outlinePaint = new Paint(1);
                    outlinePaint.setDither(true);
                    outlinePaint.setStrokeWidth(outlineSize);
                    outlinePaint.setStyle(Paint.Style.STROKE);
                    // outline /2
                    float k = size/1f;
                    // frame set
                    RectF buttonFrame = (RectF) XposedHelpers.getObjectField(_this,"mButtonFrame");
                    RectF bodyFrame = (RectF) XposedHelpers.getObjectField(_this,"mFrame");
                    // frame reducer
                    bodyFrame.set(bodyFrame.left+k,buttonFrame.bottom+k,bodyFrame.right-k,bodyFrame.bottom-k);
                    buttonFrame.set(buttonFrame.left-k,buttonFrame.top+k,buttonFrame.right+k,buttonFrame.bottom+k);
                    // draw battery outline shape
                    batteryFrame = new Path();
                    batteryFrame.reset();
                    batteryFrame.moveTo(buttonFrame.left,buttonFrame.top);
                    batteryFrame.lineTo(buttonFrame.right,buttonFrame.top);
                    batteryFrame.lineTo(buttonFrame.right,bodyFrame.top);
                    batteryFrame.lineTo(bodyFrame.right,bodyFrame.top);
                    batteryFrame.lineTo(bodyFrame.right,bodyFrame.bottom);
                    batteryFrame.lineTo(bodyFrame.left,bodyFrame.bottom);
                    batteryFrame.lineTo(bodyFrame.left,bodyFrame.top);
                    batteryFrame.lineTo(buttonFrame.left,bodyFrame.top);
                    batteryFrame.lineTo(buttonFrame.left,buttonFrame.top);
                    //save
                    XposedHelpers.setAdditionalInstanceField(_this,"mOutlinePaint",outlinePaint);
                    XposedHelpers.setAdditionalInstanceField(_this,"mOutlineFrame",batteryFrame);
                    XposedHelpers.setAdditionalInstanceField(_this,"lastWidth",width);
                    XposedHelpers.setAdditionalInstanceField(_this,"lastHeight",height);
                }
                // tinting
                Object _isDark = XposedHelpers.getAdditionalInstanceField(_this,"mDarkMode");
                boolean isDark = _isDark != null ? (boolean) _isDark : false;
                int tint = XposedHelpers.getIntField(_this,"mIconTint");
                //noinspection ResourceAsColor
                outlinePaint.setColor(isDark?(int)XposedHelpers.getAdditionalInstanceField(_this,"mDarkEdge"):(int)XposedHelpers.getAdditionalInstanceField(_this,"mLightEdge"));
                if(tint != XposedHelpers.getIntField(_this,"mLightModeFillColor") /*&& tint != XposedHelpers.getIntField(_this,"mDarkModeFillColor")*/){
                    // ignore when color
                    return;
                }
                //draw
                XposedHelpers.callMethod(param.args[0],"drawPath",new Class[]{Path.class,Paint.class},
                        batteryFrame,outlinePaint);
            }
        });
    }

    @Override
    public boolean filter() {
        return (packageName.equalsIgnoreCase("com.android.systemui") || packageName.equalsIgnoreCase("com.samsung.android.app.aodservice"))
                && use;
    }
    private int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }
}
