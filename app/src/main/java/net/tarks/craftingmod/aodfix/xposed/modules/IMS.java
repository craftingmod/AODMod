package net.tarks.craftingmod.aodfix.xposed.modules;

import android.content.Context;
import android.content.res.Resources;

import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.Module;

import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by superuser on 16/9/25.
 */

public class IMS extends Module {

    public static String[] allowedPackages = new String[]{"com.sec.imsservice","com.android.systemui"};

    @Override
    public void load() {
        if(packageName.equalsIgnoreCase("com.sec.imsservice")){
            final Class<?> ImsIconManager = XposedHelpers.findClass("com.sec.internal.ims.imsservice.ImsIconManager",loader);
            XposedHelpers.findAndHookMethod(ImsIconManager, "updateRegistrationIcon",boolean.class, new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                    try{
                        Object _this = param.thisObject;
                        Context context = (Context) XposedHelpers.getObjectField(_this,"mContext");
                        int id = getResourceId(context,
                                getVoLTEIconId((String) XposedHelpers.getObjectField(_this,"omcCode")),"drawable",context.getPackageName());
                        XposedHelpers.setIntField(_this,"mActiveVolteId",id);
                        Object statusM = XposedHelpers.getObjectField(_this,"mStatusBarManager");
                        XposedHelpers.callMethod(statusM,"setIcon",new Class[]{
                                String.class,int.class,int.class,String.class
                        },"ims_volte",id,0,"IMS Registered");

                        XposedHelpers.callMethod(statusM,"setIconVisibility",new Class[]{String.class,boolean.class},
                                "ims_volte",true);
                    }catch (SecurityException e){
                        e.printStackTrace();
                        Util.log("Security Exception");
                    }
                    return null;
                }
                private String getVoLTEIconId(String code){
                    if(config.getBoolean("OverrideTel_SKT",false)){
                        code = "SKT";
                    }else if(config.getBoolean("OverrideTel_KT",false)){
                        code = "KTT";
                    }else if(config.getBoolean("OverrideTel_LG",false)){
                        code = "LGT";
                    }
                    if ("SKT".equals(code) || "SKC".equals(code)) {
                        return "stat_sys_phone_call_skt";
                    }
                    if ("KTT".equals(code) || "KTC".equals(code)) {
                        return "stat_sys_phone_call_kt";
                    }
                    if ("LGT".equals(code) || "LUC".equals(code)) {
                        return "stat_sys_phone_call_lgt";
                    }
                    return "stat_sys_phone_call";
                }
            });
        }else if(packageName.equalsIgnoreCase("com.android.systemui")){
            final Class<?> Feature = XposedHelpers.findClass("com.android.systemui.statusbar.Feature",loader);
            String code = null;
            if(config.getBoolean("OverrideTel_SKT",false)){
                code = "SKT";
            }else if(config.getBoolean("OverrideTel_KT",false)){
                code = "KTT";
            }else if(config.getBoolean("OverrideTel_LG",false)){
                code = "LGT";
            }
            if(code != null){
                XposedHelpers.setStaticObjectField(Feature,"mStatusBarIconOpBranding",code);
            }
            final Class<?> PhoneBar = XposedHelpers.findClass("com.android.systemui.statusbar.phone.PhoneStatusBar",loader);
            XposedHelpers.findAndHookMethod(PhoneBar, "isSIMandOperatorMatched", new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam methodHookParam) throws Throwable {
                    return true;
                }
            });
        }
    }

    @Override
    public boolean filter() {
        if(packageName.equalsIgnoreCase("com.sec.imsservice")){
            if(config.getBoolean("ForceMark_HDVoice",false)){
                return true;
            }
        }else if(packageName.equalsIgnoreCase("com.android.systemui")){
            if(config.getBoolean("ForceMark_Telecom",false)){
                return true;
            }
        }
        return false;
    }

    protected IMS(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences pf) {
        super(pm,pf);
    }
    public static IMS newInstance(XC_LoadPackage.LoadPackageParam pm, XSharedPreferences pref){
        return new IMS(pm,pref);
    }
}
