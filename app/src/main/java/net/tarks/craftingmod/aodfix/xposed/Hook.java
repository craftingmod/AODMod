package net.tarks.craftingmod.aodfix.xposed;

import android.content.res.Resources;
import android.content.res.XModuleResources;
import android.content.res.XResForwarder;
import android.content.res.XResources;
import android.graphics.Color;
import android.support.annotation.Keep;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.modules.AOD;
import net.tarks.craftingmod.aodfix.xposed.modules.Font;
import net.tarks.craftingmod.aodfix.xposed.modules.IMS;
import net.tarks.craftingmod.aodfix.xposed.modules.Modelname;
import net.tarks.craftingmod.aodfix.xposed.modules.Mtp;
import net.tarks.craftingmod.aodfix.xposed.modules.SafetyBattery;
import net.tarks.craftingmod.aodfix.xposed.modules.Statusbar;
import net.tarks.craftingmod.aodfix.xposed.modules.TWTheme;
import net.tarks.craftingmod.aodfix.xposed.modules.Touch;

import java.util.ArrayList;
import java.util.Arrays;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static android.content.res.XResources.setSystemWideReplacement;
import static net.tarks.craftingmod.aodfix.api.Util.log;

/**
 * Created by superuser on 16/6/18.
 */
@Keep
public class Hook implements IXposedHookZygoteInit, IXposedHookLoadPackage, IXposedHookInitPackageResources{

    private static String MODULE_PATH = null;
    private XSharedPreferences pref;
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        final ClassLoader loader = lpparam.classLoader;
        final String packageName = lpparam.packageName.toLowerCase();


        ArrayList<String> allowedPkgs = new ArrayList<>();
        allowedPkgs.addAll(Arrays.asList(AOD.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(Font.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(IMS.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(Modelname.allowedPackages));
        //allowedPkgs.addAll(Arrays.asList(Mtp.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(Statusbar.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(TWTheme.allowedPackages));
        allowedPkgs.addAll(Arrays.asList(SafetyBattery.allowedPackages));

        boolean run = false;
        int size = allowedPkgs.size();
        for(int i=0;i<size;i+=1){
            if(packageName.equalsIgnoreCase(allowedPkgs.get(i))){
                run = true;
                break;
            }
        }
        if(!run){
            return;
        }

        pref = Util.getConfig();
        Util.debug = pref.getBoolean(Util.CONFIG_DEBUG,false);

        ArrayList<Module> modules = new ArrayList<>();
        modules.add(AOD.newInstance(lpparam,pref));
        modules.add(SafetyBattery.newInstance(lpparam,pref));
        if(Util.debug){
            modules.add(Font.newInstance(lpparam));
            modules.add(Modelname.newInstance(lpparam));
            modules.add(Statusbar.newInstance(lpparam,pref));
            modules.add(TWTheme.newInstance(lpparam));
            //modules.add(Mtp.newInstance(lpparam));
            modules.add(IMS.newInstance(lpparam,pref));
            //modules.add(Touch.newInstance(lpparam,pref));
        }
        for(int i=0;i<modules.size();i+=1){
            if(modules.get(i).filter()){
                modules.get(i).load();
            }
        }
        // ril.currentplmn : domestic
    }

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        pref = Util.getConfig();
        pref.makeWorldReadable();
        Util.debug = pref.getBoolean(Util.CONFIG_DEBUG,false);
        Util.useLog = Util.debug;
        log(pref.toString());

        MODULE_PATH = startupParam.modulePath;

        setSystemWideReplacement("android", "bool", "config_dozeAfterScreenOff", true);
        setSystemWideReplacement("android", "bool", "config_allowAutoBrightnessWhileDozing", true);
        setSystemWideReplacement("android", "bool", "config_autoBrightnessResetAmbientLuxAfterWarmUp", false);
        int bright = pref.getInt(Constants.PREF_AOD_BRIGHTNESS_VALUE,20);
        if(!pref.getBoolean(Constants.PREF_AOD_BRIGHTNESS_AUTO,false)) {
            bright = Math.min(100,Math.max(0,bright));
        }else{
            bright = 100;
        }
        //setSystemWideReplacement("android", "integer", "config_screenBrightnessDoze", bright);
        setSystemWideReplacement("android", "integer", "config_screenBrightnessDoze", -1);
    }

    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam resparam) throws Throwable {
        if (!resparam.packageName.equals("com.android.systemui") && !resparam.packageName.equals("com.samsung.android.app.aodservice")){
            return;
        }
        String pkg = resparam.packageName;
        pref = Util.getConfig();
        XModuleResources modRes = XModuleResources.createInstance(MODULE_PATH, resparam.res);
        XResources xRes = resparam.res;
        Util.log("Hello handleInit");
        if(!pref.getBoolean(Constants.PREF_AOD_BATTERY_SAFETY,false)){

        }else if(pkg.equalsIgnoreCase("com.samsung.android.app.aodservice")){
            Util.log("Chaned 1.");
            try{
                int color = modRes.getColor(R.color.battery_green_fore,null);
                Constants.COLOR_BATTERY_FILL = color;
            }catch (Resources.NotFoundException e){

            }
            xRes.setReplacement(pkg,"color","batterymeter_charge_safety_color",modRes.fwd(R.color.battery_green_fore));
           // xRes.setReplacement(pkg,"color","batterymeter_alpm_frame_inner_color",modRes.fwd(R.color.battery_green_back));
        }else if(pkg.equalsIgnoreCase("com.android.systemui")){
            Util.log("Changed 2");
            try{
                int color = modRes.getColor(R.color.battery_green_fore,null);

                Constants.COLOR_BATTERY_FILL = modRes.getColor(R.color.battery_green_fore,null);
                Constants.COLOR_BATTERY_FILL_DARK = modRes.getColor(R.color.battery_green_fore_dark,null);
                Constants.COLOR_OUTLINE_BLACK = modRes.getColor(R.color.battery_outline_dark,null);
                Constants.COLOR_OUTLINE_WHITE = modRes.getColor(R.color.battery_outline_light,null);
                Constants.COLOR_BOLT_BLACK = modRes.getColor(R.color.battery_volt_dark,null);
                Constants.COLOR_BOLT_WHITE = modRes.getColor(R.color.battery_volt_light,null);
                Constants.COLOR_BATTERY_BACK = modRes.getColor(R.color.battery_green_back,null);
                Constants.COLOR_BATTERY_BACK_DARK = modRes.getColor(R.color.battery_green_back_dark,null);
            }catch (Resources.NotFoundException e){

            }
            //xRes.setReplacement(pkg,"color","batterymeter_frame_color",greenBack);
            xRes.setReplacement(pkg,"array","batterymeter_color_values",modRes.fwd(R.array.battery_green));
            xRes.setReplacement(pkg,"array","batterymeter_color_levels",modRes.fwd(R.array.battery_level));
            xRes.setReplacement(pkg,"color","dark_mode_battery_icon_color_dual_tone_background",modRes.fwd(R.color.battery_green_back_dark));
            xRes.setReplacement(pkg,"color","light_mode_battery_icon_color_dual_tone_background",modRes.fwd(R.color.battery_green_back));
            xRes.setReplacement(pkg,"color","dark_mode_battery_icon_color_dual_tone_background",modRes.fwd(R.color.battery_green_back_dark));
            xRes.setReplacement(pkg,"color","light_mode_battery_icon_color_dual_tone_background",modRes.fwd(R.color.battery_green_back));
            xRes.setReplacement(pkg,"color","batterymeter_bolt_color",modRes.fwd(R.color.battery_volt_light));
            xRes.setReplacement(pkg,"color","batterymeter_bolt_color_light",modRes.fwd(R.color.battery_volt_dark));

            try{
                xRes.setReplacement(pkg,"dimen","status_bar_battery_width",modRes.fwd(R.dimen.battery_width));
                xRes.setReplacement(pkg,"dimen","status_bar_battery_height",modRes.fwd(R.dimen.battery_height));
                //xRes.setReplacement(pkg,"dimen","battery_margin_bottom",modRes.fwd(R.dimen.battery_marginBottom));
            }catch (Resources.NotFoundException e){

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
