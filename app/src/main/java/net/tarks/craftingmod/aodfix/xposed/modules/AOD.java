package net.tarks.craftingmod.aodfix.xposed.modules;

import android.app.Notification;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.service.notification.StatusBarNotification;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.Module;
import net.tarks.craftingmod.aodfix.xposed.aod.Alarm;
import net.tarks.craftingmod.aodfix.xposed.aod.Brighter;
import net.tarks.craftingmod.aodfix.xposed.aod.Closer;
import net.tarks.craftingmod.aodfix.xposed.aod.Detector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static net.tarks.craftingmod.aodfix.api.Util.log;

/**
 * Created by superuser on 16/6/18.
 */

public class AOD extends Module {
    public static String[] allowedPackages = new String[]{"com.samsung.android.app.aodservice"};

    private XSharedPreferences pref;
    private Class<?> AODService;
    private String pkg;
    protected AOD(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences xsp) {
        super(pm,xsp);
    }
    @Override
    public void load() {
        pkg = "com.samsung.android.app.aodservice";
        XposedHelpers.setStaticObjectField(XposedHelpers.findClass("android.os.Build",loader),"MODEL","SM-N930K");

        AODService = XposedHelpers.findClass("com.samsung.android.app.aodservice.AODService",loader);
        final Class<?> Feature = XposedHelpers.findClass("com.samsung.android.app.aodservice.utils.Feature",loader);
        pref = Util.getConfig();

        //XposedHelpers.findAndHookMethod(AODService, "aodScreenTurnedOff", Context.class, new createWindow());
        XposedHelpers.findAndHookMethod(AODService,"createWindow", new createWindow());
        XposedHelpers.findAndHookMethod(AODService, "dismissWindow", new closeWindow());

        /**
         * Notificiation color
         */
        final Class<?> AODNotiController = XposedHelpers.findClassIfExists(
                "com.samsung.android.app.aodservice.controller.notification.AODNotificationController",loader);
        if(AODNotiController != null){
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N){
                log("Hooked updateNotifications");
                XposedHelpers.findAndHookMethod(AODNotiController, "updateNotifications", View.class, new XC_MethodHook() {
                    @SuppressWarnings("deprecation")
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        final Object _this = param.thisObject;
                        if(XposedHelpers.getObjectField(_this,"mStatusBarNotifications") == null){
                            return;
                        }
                        if(XposedHelpers.getObjectField(_this,"mMissedNotiContainer") == null){
                            return;
                        }
                        // init context
                        Context context = (Context) XposedHelpers.getObjectField(_this,"mContext");
                        Resources res = context.getResources();
                        // init pref
                        XSharedPreferences pref = Util.getConfig();
                        // load config
                        boolean useColor = pref.getBoolean(Constants.PREF_AOD_NOTIFICATION_COLOR,false);
                        boolean useNum = pref.getBoolean(Constants.PREF_AOD_NOTIFICATION_NUMBER,true);
                        // eject if no
                        if(!useColor && !useNum){
                            return;
                        }
                        // init red id
                        int imagevID;
                        int RootLayoutID;
                        int countLootID;
                        int TextID;
                        int plusTextID;
                        if(XposedHelpers.getAdditionalInstanceField(_this,"mSettingLoaded") != null){
                            imagevID = (int) XposedHelpers.getAdditionalInstanceField(_this,"id_imageBack");
                            RootLayoutID = (int) XposedHelpers.getAdditionalInstanceField(_this,"id_rootLayout");
                            countLootID = (int) XposedHelpers.getAdditionalInstanceField(_this,"id_countRoot");
                            TextID = (int) XposedHelpers.getAdditionalInstanceField(_this,"id_countNum");
                            plusTextID = (int) XposedHelpers.getAdditionalInstanceField(_this,"id_countNumPlus");
                        }else{
                            imagevID = getResourceId(res,"panel_missed_noti_icon","id",pkg);
                            RootLayoutID = getResourceId(res,"dynamic_noti_layout","id",pkg);
                            countLootID = getResourceId(res,"remain_noti_count","id",pkg);
                            TextID = getResourceId(res,"noti_count","id",pkg);
                            plusTextID = getResourceId(res,"noti_count_plus","id",pkg);
                            XposedHelpers.setAdditionalInstanceField(_this,"id_imageBack",imagevID);
                            XposedHelpers.setAdditionalInstanceField(_this,"id_rootLayout",RootLayoutID);
                            XposedHelpers.setAdditionalInstanceField(_this,"id_countRoot",countLootID);
                            XposedHelpers.setAdditionalInstanceField(_this,"id_countNum",TextID);
                            XposedHelpers.setAdditionalInstanceField(_this,"id_countNumPlus",plusTextID);

                            XposedHelpers.setAdditionalInstanceField(_this,"mSettingLoaded",true);
                        }
                        // load all notifications
                        ArrayList<StatusBarNotification> noties = (ArrayList<StatusBarNotification>) XposedHelpers.getObjectField(_this,"mStatusBarNotifications");
                        // notification container
                        LinearLayout notiViews = (LinearLayout) XposedHelpers.getObjectField(_this,"mMissedNotiContainer");
                        // choose loop max
                        int max = Math.min(3,Math.min(noties.size(),notiViews.getChildCount()));
                        // define padding
                        float rightP = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,6.0f,res.getDisplayMetrics());
                        float rootRp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,6.0f,res.getDisplayMetrics());
                        float org_rightP = res.getDimensionPixelSize(getResourceId(res,"aod_notification_icon_horizontal_margin","dimen",pkg));

                        // notification loop for modify
                        Notification noti;
                        LinearLayout rootCount;
                        LinearLayout rootLayout;

                        for(int i=0;i<max;i+=1){
                            // notification
                            noti = noties.get(i).getNotification();
                            // one badge
                            View badge = notiViews.getChildAt(i);
                            // define layout
                            rootCount = (LinearLayout) badge.findViewById(countLootID);
                            rootLayout = (LinearLayout) badge.findViewById(RootLayoutID);
                            TextView countView = null;

                            // init layout structure
                            if(XposedHelpers.getAdditionalInstanceField(badge,"hasInited") == null){
                                XposedHelpers.setAdditionalInstanceField(badge,"hasInited",true);
                                // delete plus
                                badge.findViewById(plusTextID).setVisibility(View.GONE);
                                // control count right padding
                                rootCount.setPadding((int)rightP,rootCount.getPaddingTop(),rootCount.getPaddingRight(),rootCount.getPaddingBottom());
                                // set width:wrap_content, height:real height
                                rootCount.setLayoutParams(new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT,rootCount.getLayoutParams().height));
                                // get text view
                                countView = (TextView) rootCount.findViewById(TextID);
                                // set font size to 16
                                countView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);
                                // bold, to see
                                countView.setTypeface(null, Typeface.BOLD);
                                countView.setVisibility(View.VISIBLE);
                            }
                            // define should show
                            boolean showCount = noti.number > 0 && useNum;
                            boolean layoutUpdate = true;
                            // define root layout
                            LinearLayout.LayoutParams paramRoot = null;
                            // define root layout param if should change
                            if (XposedHelpers.getAdditionalInstanceField(badge, "lastState") != null && (boolean)XposedHelpers.getAdditionalInstanceField(badge, "lastState") == showCount) {
                                // use cache
                                layoutUpdate = false;
                            }
                            // notification number exists
                            if(showCount) {
                                // notification number
                                int num = Math.max(Math.min(999, noti.number), 0);
                                // get count view
                                countView = (TextView) rootCount.findViewById(TextID);
                                // set count text
                                StringBuilder sb = new StringBuilder();
                                sb.append(num);
                                if (noti.number > num) {
                                    sb.append("+");
                                }
                                countView.setText(sb.toString());
                            }
                            // should update layout?
                            if(layoutUpdate){
                                paramRoot = ((LinearLayout.LayoutParams)rootLayout.getLayoutParams());
                               if(showCount){
                                   // setting padding of root layout
                                   paramRoot.setMargins(paramRoot.leftMargin,paramRoot.topMargin,(int)rootRp,paramRoot.bottomMargin);
                                   // show count layout
                                   rootCount.setVisibility(View.VISIBLE);
                               }else{
                                   // setting padding of root layout
                                   paramRoot.setMargins(paramRoot.leftMargin,paramRoot.topMargin,(int)org_rightP,paramRoot.bottomMargin);
                                   // hide count view
                                   rootCount.setVisibility(View.GONE);
                               }
                                //modify root layout padding
                                rootLayout.setLayoutParams(paramRoot);
                            }
                            // color part
                            if(!useColor){
                                continue;
                            }
                            // get imageview and drawable
                            // just see
                            ImageView iView = (ImageView) badge.findViewById(imagevID);
                            GradientDrawable bgShape = (GradientDrawable)iView.getBackground();
                            //noinspection ResourceAsColor
                            if(//System.currentTimeMillis() - noti.when >= 1000*120 ||
                                    noti.color == Notification.COLOR_DEFAULT || noti.color == -8940887){
                                bgShape.setColor(Color.argb(77,0,0,0));
                            }else{
                                int cl = noti.color;
                                bgShape.setColor(Color.argb(188,Color.red(cl),Color.green(cl),Color.blue(cl)));
                            }
                        }
                    }
                });
            }
        }

        /**
         * Unload light
         */
        final Class<?> AODClock = XposedHelpers.findClass("com.samsung.android.app.aodservice.ui.view.AODBaseClockView",loader);
        XposedHelpers.findAndHookMethod(AODClock, "updateTime",boolean.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                Object _this = param.thisObject;
                if(XposedHelpers.getAdditionalInstanceField(_this,"mClockBright") != null){
                    Brighter bt = (Brighter) XposedHelpers.getAdditionalInstanceField(_this,"mClockBright");
                    bt.reapply();
                }
            }
        });
        // see createWindow bottom
        // Unload light
        XposedHelpers.findAndHookMethod(AODService, "unRegisterSensorListener", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                Object _this = param.thisObject;
                if(XposedHelpers.getAdditionalInstanceField(_this,"mLightListen") != null){
                    Brighter light = (Brighter) XposedHelpers.getAdditionalInstanceField(_this,"mLightListen");
                    light.destroy();
                    XposedHelpers.removeAdditionalInstanceField(_this,"mLightListen");
                }
            }
        });
    }
    private class closeWindow extends XC_MethodHook {
        @Override
        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
            log("AOD disable");
            final Object _this = param.thisObject;
            Handler handler;
            if(XposedHelpers.getAdditionalInstanceField(_this,"oHandler") != null){
                handler = (Handler) XposedHelpers.getAdditionalInstanceField(_this,"oHandler");
            }else{
                log("Handler not found");
                return;
            }
            if(XposedHelpers.getAdditionalInstanceField(_this,"mSleeper") != null){
                Detector slpDetector = (Detector) XposedHelpers.getAdditionalInstanceField(_this,"mSleeper");
                handler.removeCallbacks(slpDetector);
                XposedHelpers.removeAdditionalInstanceField(_this,"mSleeper");
            }
            if(XposedHelpers.getAdditionalInstanceField(_this,"customWaker") != null){
                Alarm waker = (Alarm) XposedHelpers.getAdditionalInstanceField(_this,"customWaker");
                waker.unwake();
                XposedHelpers.removeAdditionalInstanceField(_this,"customWaker");
            }
            if(XposedHelpers.getAdditionalInstanceField(_this,"mLightListen") != null){
                Brighter light = (Brighter) XposedHelpers.getAdditionalInstanceField(_this,"mLightListen");
                light.destroy(handler);
                XposedHelpers.removeAdditionalInstanceField(_this,"mLightListen");
            }
            if(XposedHelpers.getAdditionalInstanceField(_this,"mAutoCloser") != null){
                Closer closer = (Closer) XposedHelpers.getAdditionalInstanceField(_this,"mAutoCloser");
                handler.removeCallbacks(closer);
                XposedHelpers.removeAdditionalInstanceField(_this,"mAutoCloser");
            }
            handler.removeCallbacksAndMessages(null);
        }
    }

    private class createWindow extends XC_MethodHook {
        protected boolean checkAOD(MethodHookParam param){
            boolean run = false;
            try{
                Object dm = XposedHelpers.getObjectField(param.thisObject,"mDisplayManager");
                if((int)XposedHelpers.callMethod(XposedHelpers.callMethod(dm,"getWifiDisplayStatus"),"getActiveDisplayState") != 2){
                    run = true;
                }
            }catch (NoSuchFieldError e){
                run = true;
            }
            return run;
        }
        @Override
        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
            log("AOD enable");
            pref = Util.getConfig();
            final Object _this = param.thisObject;

            /**
             * Handler init
             */
            Handler handler;
            if(XposedHelpers.getAdditionalInstanceField(_this,"oHandler") == null){
                handler = new Handler(((Context)_this).getMainLooper());
                XposedHelpers.setAdditionalInstanceField(_this,"oHandler",handler);
            }else{
                handler = (Handler) XposedHelpers.getAdditionalInstanceField(_this,"oHandler");
            }
            /**
             * Sleep detect
             */
            if(!pref.getBoolean(Constants.PREF_AOD_NATIVE_WAKELOCK,false)){
                Detector slpDetector;
                if(XposedHelpers.getAdditionalInstanceField(_this,"mSleeper") == null){
                    slpDetector = new Detector(_this);
                    XposedHelpers.setAdditionalInstanceField(_this,"mSleeper",slpDetector);
                }else{
                    slpDetector = (Detector) XposedHelpers.getAdditionalInstanceField(_this,"mSleeper");
                }
                slpDetector.reset(handler);
                handler.postDelayed(slpDetector,1500);
            }
            /**
             * Module: bright
             */
            if(pref.getBoolean(Constants.PREF_AOD_BRIGHTNESS_AUTO,false)){
                Brighter light;
                if(XposedHelpers.getAdditionalInstanceField(_this,"mLightListen") == null){
                    light = new Brighter(_this);
                    XposedHelpers.setAdditionalInstanceField(_this,"mLightListen",light);
                }else{
                    light = (Brighter) XposedHelpers.getAdditionalInstanceField(_this,"mLightListen");
                }
                light.reset(handler);
                handler.postDelayed(light,1500);

                SensorManager sensorManager = (SensorManager) XposedHelpers.getObjectField(_this,"mSensorManager");
                Sensor sensorL = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
                if(sensorL != null){
                    sensorManager.registerListener(light,sensorL,SensorManager.SENSOR_DELAY_NORMAL);
                }else{
                    log("RIP Bright sensor");
                }
            }
            /**
             * Module : Waker
             */
            //getWifi
            ConnectivityManager conM;
            if(XposedHelpers.getAdditionalInstanceField(_this,"conManager") == null){
                conM = (ConnectivityManager) XposedHelpers.callMethod(_this,"getSystemService",new Class[]{String.class},Context.CONNECTIVITY_SERVICE);
                XposedHelpers.setAdditionalInstanceField(_this,"conManager",conM);
            }else{
                conM = (ConnectivityManager) XposedHelpers.getAdditionalInstanceField(_this,"conManager");
            }
            NetworkInfo networks = conM.getActiveNetworkInfo();
            int dura;
            boolean inf;
            if(networks != null && networks.getType() == ConnectivityManager.TYPE_WIFI){
                // Connected wifi.
                dura = pref.getInt(Constants.PREF_AOD_DURATION_WIFI,60*15);
                inf = pref.getBoolean(Constants.PREF_AOD_DURATION_WIFI_INFINITY,true);
            }else{
                // Data connected.
                dura = pref.getInt(Constants.PREF_AOD_DURATION_MOBILE,60*15);
                inf = pref.getBoolean(Constants.PREF_AOD_DURATION_MOBILE_INFINITY,true);
            }
            // Real wake
            if(!pref.getBoolean(Constants.PREF_AOD_NATIVE_WAKELOCK,false)){
                //Init Wakelock
                PowerManager pm;
                if(XposedHelpers.getAdditionalInstanceField(_this,"powerManager") == null){
                    pm = (PowerManager) XposedHelpers.callMethod(_this,"getSystemService",new Class[]{String.class},Context.POWER_SERVICE);
                    XposedHelpers.setAdditionalInstanceField(_this,"powerManager",pm);
                }else{
                    pm = (PowerManager) XposedHelpers.getAdditionalInstanceField(_this,"powerManager");
                }
                //Init Alarm
                Alarm waker;
                if(XposedHelpers.getAdditionalInstanceField(_this,"customWaker") == null){
                    waker = new Alarm((Context) _this,pm);
                    XposedHelpers.setAdditionalInstanceField(_this,"customWaker",waker);
                }else{
                    waker = (Alarm) XposedHelpers.getAdditionalInstanceField(_this,"customWaker");
                }
                if(inf){
                    waker.wakeInf();
                }else{
                    waker.wake(dura);
                }
            }else{
                if(!inf){
                    // use native so close after time
                    Closer closer;
                    if(XposedHelpers.getAdditionalInstanceField(_this,"mAutoCloser") == null){
                        closer =  new Closer(_this);
                        XposedHelpers.setAdditionalInstanceField(_this,"mAutoCloser",closer);
                    }else{
                        closer = (Closer) XposedHelpers.getAdditionalInstanceField(_this,"mAutoCloser");
                    }
                    closer.reset(handler);
                    handler.postAtTime(closer,SystemClock.uptimeMillis() + dura * 1000);
                }
            }
            /**
             * Module: Light
             */
            Object clock = XposedHelpers.getObjectField(param.thisObject,"mAODBaseClockView");
            Object lighter = XposedHelpers.getAdditionalInstanceField(param.thisObject,"mLightListen");
            if(clock != null && lighter != null){
                log("Registered brightListener");
                XposedHelpers.setAdditionalInstanceField(clock,"mClockBright",lighter);
                ((Brighter)lighter).reapply(XposedHelpers.getObjectField(clock,"mRootView"));
            }
            /**
             * Module: Knockon
             */
            if(clock != null && pref.getBoolean(Constants.PREF_AOD_KNOCK_ON,false)){
                View layout = (View)clock;
                layout.setOnClickListener(new View.OnClickListener() {
                    private long lastTime = 0;
                    private Context context = (Context)_this;
                    @SuppressWarnings("deprecation")
                    @Override
                    public void onClick(View view) {
                        long time = SystemClock.elapsedRealtime();
                        if(time - lastTime < 500){
                            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                            PowerManager.WakeLock wl = pm.newWakeLock(
                                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                                            PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                            PowerManager.ON_AFTER_RELEASE, "knockonWake");
                            wl.acquire();
                            wl.release();
                        }
                        lastTime = time;
                    }
                });
            }

        }
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.samsung.android.app.aodservice");
    }

    public static AOD newInstance(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences xs){
        return new AOD(pm,xs);
    }
}
