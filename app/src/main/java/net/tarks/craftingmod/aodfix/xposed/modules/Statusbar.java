package net.tarks.craftingmod.aodfix.xposed.modules;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Message;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.mikepenz.crossfader.util.UIUtils;

import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.Module;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static net.tarks.craftingmod.aodfix.api.Util.log;

/**
 * Created by superuser on 16/6/18.
 */

public class Statusbar extends Module {

    public static String[] allowedPackages = new String[]{"com.android.systemui"};
    public boolean goodLock;

    @Override
    public void load() {

        /**
         * Real Signature
         */
        final String pkg = "com.android.systemui";
        final Class<?> Feature = XposedHelpers.findClassIfExists("com.android.systemui.statusbar.Feature",loader);
        //XposedHelpers.setStaticBooleanField(Feature,"mUsePureSignalCluster",true);
        if(Feature != null){
            goodLock = false;
            XposedHelpers.setStaticBooleanField(Feature,"mUseHSDPADataIcon",true);
        }else{
            goodLock = true;
        }

        /**
         * 방해금지 updateVolumeZenTW
         */
        final Class<?> PhoneStatusPolicy = XposedHelpers.findClass("com.android.systemui.statusbar.phone.PhoneStatusBarPolicy",loader);
        final Class<?> UserHandle = XposedHelpers.findClass("android.os.UserHandle",loader);
        if(!goodLock && XposedHelpers.findMethodExactIfExists(PhoneStatusPolicy,"updateVolumeZenTW") != null) {
            XposedHelpers.findAndHookMethod(PhoneStatusPolicy, "updateVolumeZenTW", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Context context = (Context) XposedHelpers.getObjectField(param.thisObject,"mContext");

                    boolean vzw = XposedHelpers.getStaticBooleanField(Feature,"mUseDndNotificationForVZW");
                    int id = getResourceId(context,vzw?"stat_notify_dormant_mode_vzw":"stat_notify_dormant_mode","drawable","com.android.systemui");

                    Object nManager = XposedHelpers.getObjectField(param.thisObject,"mNotificationManager");
                    XposedHelpers.callMethod(nManager,"cancelAsUser",new Class[]{String.class,int.class, UserHandle},
                            "noti_DoNotDisturb",id, XposedHelpers.getStaticObjectField(UserHandle,"ALL"));
                    // this.mNotificationManager.cancelAsUser("noti_DoNotDisturb", i2, UserHandle.ALL);
                }
            });
        }
        /**
         * Storage
         */
        final Class<?> volumeInfo = XposedHelpers.findClass("android.os.storage.VolumeInfo",loader);
        final Class<?> StorageNoti = XposedHelpers.findClass("com.android.systemui.usb.StorageNotification",loader);
        if(!goodLock){
            XposedHelpers.findAndHookMethod(StorageNoti, "onPublicVolumeStateChangedInternal", volumeInfo, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    if(param.args[0] != null){
                        Object volumeInfo = param.args[0];
                        Object nManager = XposedHelpers.getObjectField(param.thisObject,"mNotificationManager");
                        XposedHelpers.callMethod(nManager,"cancelAsUser",new Class[]{String.class,int.class, UserHandle},
                                XposedHelpers.callMethod(volumeInfo,"getId"),1397773634, XposedHelpers.getStaticObjectField(UserHandle,"ALL"));
                        // this.mNotificationManager.cancelAsUser(str, 1397773634, UserHandle.ALL);
                    }
                }
            });
        }else{
            //goodLock
            final Class<?> rContainer = XposedHelpers.findClass("com.android.systemui.reflection.ReflectionContainer",loader);
            XposedHelpers.findAndHookMethod(StorageNoti, "onPublicVolumeStateChangedInternal", Object.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    //  ReflectionContainer.getNotificationManager().notifyAsUser(this.mNotificationManager, ReflectionContainer.getVolumeInfo().getId(vol), PUBLIC_ID, notif, ReflectionContainer.getUserHandle().ALL);
                    Object rnManager = XposedHelpers.callStaticMethod(rContainer,"getNotificationManager");
                    Object rVolumeInfo = XposedHelpers.callStaticMethod(rContainer,"getVolumeInfo");
                    // no reflection manager
                    Object nManager = XposedHelpers.getObjectField(param.thisObject,"mNotificationManager");
                    // public String getId(Object instance)
                    Object _id = XposedHelpers.callMethod(rVolumeInfo,"getId",new Class[]{Object.class},param.args[0]);
                    // public String getFsUuid(Object instance)
                    Object _fsUuid = XposedHelpers.callMethod(rVolumeInfo,"getFsUuid",new Class[]{Object.class},param.args[0]);
                    // UserHandle.ALL
                    Object UserHandle_ALL = XposedHelpers.getStaticObjectField(UserHandle,"ALL");
                    // Public ID
                    int PUBLIC_ID = XposedHelpers.getStaticIntField(StorageNoti,"PUBLIC_ID");
                    //cancelAsUser(Object notificationManagerObj, String tag, int id, UserHandle user)
                    if(_id != null && _fsUuid != null){
                        String id = (String)_id;
                        // ReflectionContainer.getNotificationManager().cancelAsUser(
                        // this.mNotificationManager, ReflectionContainer.getVolumeInfo().getId(vol), PUBLIC_ID, UserHandle.ALL);
                        XposedHelpers.callMethod(rnManager,"cancelAsUser",new Class[]{Object.class,String.class,int.class,UserHandle},
                                nManager,id,PUBLIC_ID,UserHandle_ALL);
                    }
                }
            });
        }

        /**
         * Network speed indicator
         */
        final Class<?> NetspeedView = XposedHelpers.findClassIfExists("com.android.systemui.statusbar.policy.NetspeedView",loader);
        if(NetspeedView != null && !config.getBoolean(Constants.PREF_AOD_NATIVE_WAKELOCK,false)){
            // no good lock
            XposedBridge.hookAllConstructors(NetspeedView, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    TextView _this = (TextView) param.thisObject;
                    //original: 15dip
                    _this.setTextSize(TypedValue.COMPLEX_UNIT_DIP,13);
                }
            });
            final Class<?> NetManager = XposedHelpers.findClassIfExists("com.android.systemui.statusbar.policy.NetspeedView$NetworkSpeedManager",loader);
            final Class<?> NetHandler = XposedHelpers.findClassIfExists("com.android.systemui.statusbar.policy.NetspeedView$NetworkSpeedManager$1",loader);
            if(NetHandler != null && NetManager != null){
                XposedHelpers.findAndHookConstructor(NetHandler, NetManager, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        if(param.args[0] != null){
                            XposedHelpers.setAdditionalInstanceField(param.thisObject,"mThis",param.args[0]);
                            log("Inserted NetworkSpeedManager to mThis");
                        }
                    }
                });
                XposedHelpers.findAndHookMethod(NetHandler, "handleMessage", Message.class, new XC_MethodHook() {
                    private String convert(String text){
                        if(text.equalsIgnoreCase("0 K/s")){
                            return "";
                        }else{
                            return text.replace("/s"," ");
                        }
                    }
                    private String convert(CharSequence text){
                        return convert(text.toString());
                    }
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Message msg = (Message) param.args[0];
                        Object _this;
                        if(XposedHelpers.getAdditionalInstanceField(param.thisObject,"mThis") != null){
                            _this = XposedHelpers.getAdditionalInstanceField(param.thisObject,"mThis");
                        }else{
                            return;
                        }
                        if(msg.what == 1){
                            Iterator it = (Iterator) XposedHelpers.callMethod(XposedHelpers.getObjectField(_this,"observers"),
                                    "iterator");
                            TextView tv;
                            while (it.hasNext()){
                                tv = (TextView) it.next();
                                tv.setText(convert(tv.getText()));
                            }
                        }
                    }
                });
            }
        }

    }

    public static String toString(int i){
       return String.format("%d", new Object[] { Integer.valueOf(i) });
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.android.systemui");
    }

    protected Statusbar(XC_LoadPackage.LoadPackageParam pm, XSharedPreferences pref) {
        super(pm,pref);
    }
    public static Statusbar newInstance(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences pf){
        return new Statusbar(pm,pf);
    }

    public static int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }
}
