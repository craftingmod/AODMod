package net.tarks.craftingmod.aodfix.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.ui.base.BaseFragment;

/**
 * Created by jhrunning on 01/10/2016.
 */

public class BlankFragment extends BaseFragment {
    public static final String POSITION = "position";
    private View rootView;
    private String param;

    public BlankFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.hello_frag, container, false);
        param = getArguments().getString(POSITION);


        TextView textView = (TextView) rootView.findViewById(R.id.hello_label);
        textView.setText(param);
        return rootView;
    }
}
