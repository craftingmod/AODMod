package net.tarks.craftingmod.aodfix;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.artitk.licensefragment.model.License;
import com.artitk.licensefragment.model.LicenseType;
import com.artitk.licensefragment.support.v4.ScrollViewLicenseFragment;
import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialize.util.UIUtils;

import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.IFragmentSupply;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.AODNotiFragment;
import net.tarks.craftingmod.aodfix.ui.BlankFragment;
import net.tarks.craftingmod.aodfix.ui.ExpFragment;
import net.tarks.craftingmod.aodfix.ui.IBController;
import net.tarks.craftingmod.aodfix.ui.ISaver;
import net.tarks.craftingmod.aodfix.ui.InfoFragment;
import net.tarks.craftingmod.aodfix.ui.LightFragment;
import net.tarks.craftingmod.aodfix.ui.TimePickerFragment;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by superuser on 16/9/30.
 */

public class Main extends AppCompatActivity implements Drawer.OnDrawerItemClickListener, IFragmentSupply, ISaver, IBController, OnCheckedChangeListener {
    private final int BLANK = 12;
    private final int FIRST = 2;
    private final int ONTIME_WIFI = 7;
    private final int ONTIME_MOBILE = 8;
    private final int MENU_LICENSE = 3;
    private final int SAVE = 26;
    private final int EXIT = 31;
    private final int BRIGHT = 37;
    private final int AODSCREEN = 42;
    private final int EXPERIMENT = 47;
    private final int UPDATECHECK = 51;
    private final int SAFEBETTERY = 72;
    private final int INTERNALWAKE = 84;
    private final int[] SORT = new int[]{0,BLANK,FIRST,ONTIME_WIFI,ONTIME_MOBILE,BRIGHT,AODSCREEN,EXPERIMENT,MENU_LICENSE};
    private final int[] IGNORE_SMALL = new int[]{ONTIME_MOBILE,ONTIME_WIFI};

    // 뷰페이저
    private ViewPager mViewPager;
    private PagerAdapter mAdapter;
    private Toolbar toolbar;
    private LinearLayout rootLayout;

    // 드로어들
    private Drawer extendedDrawer;
    private AccountHeader drawerHeader;
    // 아이템들
    private PrimaryDrawerItem itemSave;
    private SecondaryDrawerItem itemWifi;
    private SecondaryDrawerItem itemMobile;
    private PrimaryDrawerItem itemBright;
    private SwitchDrawerItem itemExp;
    private SwitchDrawerItem itemBattery;
    private SwitchDrawerItem itemNativeWake;

    // 기본
    private Context context;

    protected DrawerBuilder makeDrawerMenu(DrawerBuilder builder){
        SharedPreferences pref = Util.getConfig(context);
        Util.log(getColor(R.color.battery_green_fore) + "");

        itemSave = new PrimaryDrawerItem().withName(R.string.saveText).withIcon(GoogleMaterial.Icon.gmd_save).withTag("Save").withSelectable(false).withIdentifier(SAVE).withEnabled(true);
        itemWifi = new SecondaryDrawerItem().withName(R.string.drawer_menu_wakelock_wifi).withLevel(2).withIcon(GoogleMaterial.Icon.gmd_wifi).withIdentifier(ONTIME_WIFI);
        itemMobile = new SecondaryDrawerItem().withName(R.string.drawer_menu_wakelock_mobile).withLevel(2).withIcon(GoogleMaterial.Icon.gmd_network_cell).withIdentifier(ONTIME_MOBILE);
        itemBright = new PrimaryDrawerItem().withName(R.string.drawer_menu_bright).withIcon(GoogleMaterial.Icon.gmd_settings_brightness).withIdentifier(BRIGHT);
        itemExp = new SwitchDrawerItem().withName(R.string.drawer_menu_exp_debug).withIcon(GoogleMaterial.Icon.gmd_verified_user).withIdentifier(EXPERIMENT)
                .withChecked(pref.getBoolean(Constants.PREF_DEBUG,false)).withOnCheckedChangeListener(this);
        itemBattery = new SwitchDrawerItem().withName(R.string.drawer_menu_exp_battery).withIcon(GoogleMaterial.Icon.gmd_battery_full).withSelectable(false)
                .withChecked(pref.getBoolean(Constants.PREF_AOD_BATTERY_SAFETY,false)).withIconColorRes(R.color.battery_green_fore).withIdentifier(SAFEBETTERY)
                .withEnabled(true).withOnCheckedChangeListener(this);
        itemNativeWake = new SwitchDrawerItem().withName(R.string.drawer_menu_exp_native).withIcon(GoogleMaterial.Icon.gmd_work).withSelectable(false)
                .withChecked(pref.getBoolean(Constants.PREF_AOD_NATIVE_WAKELOCK,false)).withIdentifier(INTERNALWAKE)
                .withEnabled(true).withOnCheckedChangeListener(this);

        return builder.addDrawerItems(
                new ExpandableDrawerItem().withName(R.string.drawer_menu_wakelock_title).withIcon(GoogleMaterial.Icon.gmd_timer).withSelectable(false).withIsExpanded(true).withSubItems(
                        itemWifi,itemMobile
                ),
                itemBright,
                new PrimaryDrawerItem().withName(R.string.drawer_menu_aodscreen).withIcon(GoogleMaterial.Icon.gmd_watch).withIdentifier(AODSCREEN),
                new SectionDrawerItem().withName(R.string.drawer_menu_exp_title),
                itemExp,
                itemBattery,
                itemNativeWake,
                new SectionDrawerItem().withName(R.string.drawer_menu_subtitile_etc),
                new PrimaryDrawerItem().withName(R.string.drawer_menu_updateCheck).withSelectable(false).withIcon(GoogleMaterial.Icon.gmd_system_update_alt).withIdentifier(UPDATECHECK),
                new SecondaryDrawerItem().withName(R.string.drawer_menu_opensource_title).withIcon(FontAwesome.Icon.faw_github).withIdentifier(MENU_LICENSE)
        ).addStickyDrawerItems(
                new PrimaryDrawerItem().withName(R.string.drawer_menu_info).withIcon(GoogleMaterial.Icon.gmd_info).withIdentifier(FIRST),
                new PrimaryDrawerItem().withName(R.string.drawer_menu_exit).withIcon(GoogleMaterial.Icon.gmd_exit_to_app).withSelectable(false).withIdentifier(EXIT)
        );
    }
    /**
     * Fragment 제공자
     */
    @Override
    public Fragment getItem(int identify) {
        Fragment fragment = null;
        switch (identify){
            case BLANK:
                break;
            case FIRST:
                fragment = InfoFragment.newInstance();
                break;
            case ONTIME_MOBILE:
                fragment = TimePickerFragment.newInstance(Constants.PREF_AOD_DURATION_MOBILE);
                break;
            case ONTIME_WIFI:
                fragment = TimePickerFragment.newInstance(Constants.PREF_AOD_DURATION_WIFI);
                break;
            case BRIGHT:
                fragment = LightFragment.newInstance();
                break;
            case AODSCREEN:
                fragment = AODNotiFragment.getInstance();
                break;
            case EXPERIMENT:
                fragment = ExpFragment.newInstance();
                break;
            case MENU_LICENSE: // MENU_LICENSE
                ArrayList<License> custom = new ArrayList<>();
                custom.add(new License(context, "AppUpdater", LicenseType.APACHE_LICENSE_20, "2016","Javier Santos"));
                custom.add(new License(context, "libsuperuser", LicenseType.APACHE_LICENSE_20, "2012-2015","Jorrit Chainfire Jongma"));
                custom.add(new License(context, "Xposed Framework", LicenseType.APACHE_LICENSE_20, "2013","rovo89 and Tungstwenty"));
                custom.add(new License(context, "Android", LicenseType.APACHE_LICENSE_20, "2013","The Android Open Source Project"));
                custom.add(new License(context, "CircleSeekbar", LicenseType.APACHE_LICENSE_20, "2014","Gustavo Claramunt (Ander Webbs)"));
                custom.add(new License(context, "license-fragment", LicenseType.APACHE_LICENSE_20, "2015","Artit Kiuwilai"));
                custom.add(new License(context, "SwitchButton", LicenseType.APACHE_LICENSE_20, "2015","kyleduo"));
                custom.add(new License(context, "MaterialDrawer", LicenseType.APACHE_LICENSE_20, "2016","Mike Penz"));
                custom.add(new License(context, "crossfader", LicenseType.APACHE_LICENSE_20, "2016","Mike Penz"));
                ScrollViewLicenseFragment frag = ScrollViewLicenseFragment.newInstance();
                frag.addCustomLicense(custom);
                fragment = frag;
                break;
        }
        // 예외 처리
        if(fragment == null){
            fragment = new BlankFragment();
            Bundle bundle = new Bundle();
            bundle.putString(BlankFragment.POSITION,identify+"");
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    /**
     * 액션바 타이틀
     */
    /*
        <string name="fragment_title_wifi">WIFI에서 켜지는 시간</string>
    <string name="fragment_title_mobile">데이터에서 켜지는 시간</string>
    <string name="fragment_title_bright">화면 밝기</string>
    <string name="fragment_title_aodinscreen">시계 화면</string>
    <string name="fragment_title_infomation">정보</string>
    <string name="fragment_title_openL">열린소스 라이선스</string>
     */
    @Override
    public String getTitle(int identify) {
        String title = null;
        switch (identify){
            case BLANK:
                title = "Blank";
                break;
            case ONTIME_WIFI:
                title = getString(R.string.fragment_title_wifi);
                break;
            case ONTIME_MOBILE:
                title = getString(R.string.fragment_title_mobile);
                break;
            case BRIGHT:
                title = getString(R.string.fragment_title_bright);
                break;
            case AODSCREEN:
                title = getString(R.string.fragment_title_aodinscreen);
                break;
            case FIRST:
                title = getString(R.string.fragment_title_infomation);
                break;
            case EXPERIMENT:
                title = getString(R.string.fragment_title_exp);
                break;
            case MENU_LICENSE: // MENU_LICENSE
                title = getString(R.string.fragment_title_openL);
                break;
        }
        if(title == null){
            title = "my neckless";
        }
        return title;
    }

    /**
     * Viewpager init
     * @param savedInstanceState
     */
    // http://stackoverflow.com/questions/18743124/make-main-content-area-active-in-drawerlayout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        boolean isDebug = false;
        try {
            String vName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            isDebug = vName.contains("debug");
            Util.useLog = isDebug;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if(!isDebug && !checkAOD()){
            return;
        }

        setContentView(R.layout.main);
        /**
         * 액션바 활성화
         */
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ㅎㅇ 세계");
        /**
         * 헤더
         */

        drawerHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(new ColorDrawable(Color.parseColor("#FDFDFD")))
                .withHeightPx(UIUtils.getActionBarHeight(this))
                .withAccountHeader(R.layout.material_drawer_header)
                .withTextColor(Color.BLACK)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(
                        new ProfileDrawerItem().withName(getString(R.string.drawer_header_message)).withIcon(R.drawable.ic_profile)
                )
                .withSavedInstance(savedInstanceState)
                .build();


        DrawerBuilder builder = new DrawerBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withShowDrawerOnFirstLaunch(true)
                .withAccountHeader(drawerHeader);
        extendedDrawer = makeDrawerMenu(builder)
                .withOnDrawerItemClickListener(this)
                .withActionBarDrawerToggle(true)
                //.withGenerateMiniDrawer(true)
                .withSavedInstance(savedInstanceState)
                .build();

        //smallDrawer = extendedDrawer.getMiniDrawer().withIncludeSecondaryDrawerItems(true);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        /**
         * 뷰 페 이 저
         */
        mViewPager = (ViewPager) findViewById(R.id.container);
        // 아답터
        mAdapter = new PagerAdapter(getSupportFragmentManager(),this);
        mViewPager.setAdapter(mAdapter);
        rootLayout = (LinearLayout) findViewById(R.id.root_Main);

        // 세이버
        toSave = new HashMap<>();
        changed = false;
        /**
         * 넖게넖게
         */
        DrawerLayout mDrawerLayout = extendedDrawer.getDrawerLayout();
        Field mDragger = null;//mRightDragger for right obviously
        try {
            mDragger = mDrawerLayout.getClass().getDeclaredField(
                    "mLeftDragger");
            mDragger.setAccessible(true);
            ViewDragHelper draggerObj = (ViewDragHelper) mDragger
                    .get(mDrawerLayout);

            Field mEdgeSize = draggerObj.getClass().getDeclaredField(
                    "mEdgeSize");
            mEdgeSize.setAccessible(true);
            mEdgeSize.setInt(draggerObj, (int) UIUtils.convertDpToPixel(60,context)); //optimal value as for me, you may set any constant in dp
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        updateAllMenu();
        extendedDrawer.setStickyFooterSelection(FIRST,true);

        checkDailyUpdate();
    }
    public void checkDailyUpdate(){
        SharedPreferences pref = Util.getConfig(context);
        if(System.currentTimeMillis() - pref.getLong(Constants.PREF_LAST_UPDATE_CHECK,0L) >= 3600 * 1000 * 10){
            AppUpdater updater = checkUpdate(true);
            if(updater != null){
                pref.edit().putLong(Constants.PREF_LAST_UPDATE_CHECK,System.currentTimeMillis()).apply();
                updater.start();
            }
        }
    }
    public boolean checkAOD(){
        int result = usableADD(context);
        if(result == 0){
            return true;
        }
        if(extendedDrawer != null){
            extendedDrawer.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.aod_unusable_notify_title);
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.aod_unusable_notify_content_frame));
        int[] code = new int[]{1,2,4,8,16};
        int[] msgID = new int[]{R.string.aod_unusable_notify_content_aodvold,R.string.aod_unusable_notify_content_aodold,
        R.string.aod_unusable_notify_content_aodvold,R.string.aod_unusable_notify_content_systemframe,
        R.string.aod_unusable_notify_content_xposed};
        for(int i=0;i<code.length;i+=1){
            if((result & code[i]) == code[i]){
                //code[i] unavailable
                sb.append(System.lineSeparator());
                sb.append(getString(msgID[i]));
            }
        }
        builder.setMessage(sb.toString());
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.aod_unusable_notify_button, (d, i) -> {
            finishAffinity();
        });
        builder.create();
        builder.show();
        return false;
    }
    private int usableADD(Context con){
        int out = 0;
        //First. version check
        String version;
        try {
            PackageInfo pInfo = con.getPackageManager().getPackageInfo("com.samsung.android.app.aodservice", 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "-1";
        }
        String[] vSplit = version.split("\\.");
        try{
            if(vSplit.length < 2){
                out += 1; // 0001
            }else if(Integer.parseInt(vSplit[0]) == 1 && Integer.parseInt(vSplit[1]) <= 6){
                out += 2; // 0010
            }
        }catch (NumberFormatException | ArrayIndexOutOfBoundsException e){
            Util.log(Log.getStackTraceString(e));
            out += 4; // 0100
        }
        //Second. framework check
        try {
            Class.forName("com.samsung.android.aod.AlwaysOnDisplayService");
        } catch (ClassNotFoundException e) {
            out += 8; // 1000
        }
        //Third. xposed check
        try{
            throw new Exception("Check");
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String logS = sw.toString();
            if(!logS.contains("de.robv.android.xposed.XposedBridge")){
                out += 16; // 1 0000
            }
        }
        Util.log((out & 16) + "");
        return out;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            if(extendedDrawer.isDrawerOpen()){
                extendedDrawer.closeDrawer();
            }else{
                extendedDrawer.openDrawer();
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        if(extendedDrawer != null){
            outState = extendedDrawer.saveInstanceState(outState);
            outState = drawerHeader.saveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (extendedDrawer != null && !extendedDrawer.isDrawerOpen()) {
            extendedDrawer.openDrawer();
        }
    }
    private int lastSelectedID = -1;
    /**
     * 메뉴 클릭하였을때 반응
     * @param view 네비게이션 드로어
     * @param position 위치 (의미없음
     * @param drawerItem 선택된 아이템(null일수도 있음)
     * @return
     */
    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        if(drawerItem == null){
            return true;
        }
        if(drawerItem.getIdentifier() == SAVE){
            flush();
            return false;
        }
        if(drawerItem.getIdentifier() == EXIT){
            finishAffinity();
            return true;
        }
        if(drawerItem.getIdentifier() == UPDATECHECK){
            AppUpdater updater = checkUpdate(false);
            if(updater != null){
                updater.start();
            }
            return false;
        }
        int ps = 0;
        boolean hide = false;
        int id = (int)drawerItem.getIdentifier();
        for(int i=0;i<SORT.length;i+=1){
            if(id == SORT[i]){
                ps = i;
            }
            if(i<IGNORE_SMALL.length && id == IGNORE_SMALL[i]){
                hide = true;
            }
        }
        if(ps == 0){
            return true;
        }

        lastSelectedID = (int) drawerItem.getIdentifier();
        getSupportActionBar().setTitle(getTitle(id));
        mViewPager.setCurrentItem(ps,false);
        return false;
    }
    @Override
    public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
        int id = (int) drawerItem.getIdentifier();
        String saveID = null;
        switch (id){
            case EXPERIMENT:
                saveID = Constants.PREF_DEBUG;
                break;
            case SAFEBETTERY:
                saveID = Constants.PREF_AOD_BATTERY_SAFETY;
                break;
            case INTERNALWAKE:
                saveID = Constants.PREF_AOD_NATIVE_WAKELOCK;
                break;
        }
        if(saveID != null){
            this.request(saveID,isChecked);
            this.updateSave();
        }
    }

    @Override
    public int[] getIdentifier() {
        return SORT;
    }

    private AppUpdater checkUpdate(boolean auto){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            //pref.edit().putLong("lastUpdate",now).apply();
            if(!auto){
                Snackbar.make(rootLayout,"업데이트 체크 중입니다.",Snackbar.LENGTH_SHORT).show();
            }
            AppUpdater updater = new AppUpdater(this);
            updater.setDisplay(Display.DIALOG)
                    .setUpdateFrom(UpdateFrom.XML)
                    .setUpdateXML("https://dl.dropboxusercontent.com/u/94271045/AODMod/update.xml")
                    .setTitleOnUpdateAvailable("업데이트가 있습니다.")
                    .setContentOnUpdateAvailable("$VERSION" + " 버전이 있어요.\n다운로드가 완료되면 상단바를 내려 설치해 주세요.\n\n$NOTE")
                    .setTitleOnUpdateNotAvailable("업데이트가 없습니다.")
                    .setContentOnUpdateNotAvailable("정말 가끔씩만 업데이트 됩니다.")
                    .setButtonUpdate("업뎃")
                    .setButtonDismiss("안하기")
                    .showAppUpdated(!auto)
                    .setButtonDoNotShowAgain("ㅗ");
            return updater;
        }else{
            if(!auto){
                Snackbar.make(rootLayout,"권한을 허용해주시고 다시 해주세요.",Snackbar.LENGTH_LONG).show();
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},5313);
            }
            return null;
        }
    }

    /**
     * Pref saver
     * @param id
     * @param value
     */
    private HashMap<String,Object> toSave;
    private boolean changed;
    private boolean save_added = false;

    private void updateSaveButton(){
        if(changed){
            if(!save_added){
                extendedDrawer.addStickyFooterItemAtPosition(itemSave,0);
                save_added = true;
            }
        }else{
            if(save_added){
                extendedDrawer.removeStickyFooterItemAtPosition(0);
                save_added = false;
            }
        }
        //extendedDrawer.getDrawerItem("Save").withEnabled(changed);
    }
    private void updateAllMenu(){
        SharedPreferences pref = Util.getConfig(context);
        String textWifi = getString(R.string.drawer_menu_wakelock_wifi);
        String textM = getString(R.string.drawer_menu_wakelock_mobile);
        String textBright = getString(R.string.drawer_menu_bright);
        extendedDrawer.updateName(ONTIME_WIFI, new StringHolder(
                textWifi.replace("%p",
                        Util.parseDuration(pref,context,Constants.PREF_AOD_DURATION_WIFI)
                )));
        extendedDrawer.updateName(ONTIME_MOBILE, new StringHolder(
                textM.replace("%p",
                       Util.parseDuration(pref,context,Constants.PREF_AOD_DURATION_MOBILE)
                )));
        String bright = Util.parseBrightness(pref,context);
        if(pref.getBoolean(Constants.PREF_AOD_NATIVE_WAKELOCK,false)){
            if(Settings.Secure.getInt(context.getContentResolver(),"conf_aod_autobright",0) == 1){
                bright = getString(R.string.bright_text_auto);
            }else{
                int b = Settings.Secure.getInt(context.getContentResolver(),"conf_aod_bright",-1);
                bright = b>=0?String.valueOf(b):bright;
            }
            extendedDrawer.updateItem(itemBright.withEnabled(false));
        }else{
            extendedDrawer.updateItem(itemBright.withEnabled(true));
        }
        extendedDrawer.updateName(BRIGHT, new StringHolder(
                textBright.replace("%p",bright)
        ));
        extendedDrawer.updateItem(itemExp.withChecked(pref.getBoolean(Constants.PREF_DEBUG,false)));

        updateSaveButton();
    }
    @Override
    public void request(String id, Object value) {
        changed = true;
        toSave.put(id,value);
    }

    @Override
    public void requestTitleChange(int id, String title) {

    }

    @Override
    public void updateSave() {
        updateSaveButton();
    }

    @Override
    public void flush() {
        SharedPreferences pref = Util.getConfig(context);
        SharedPreferences.Editor edit = pref.edit();

        for(Map.Entry<String, Object> entry : toSave.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();
            if(value instanceof String){
                Util.log(key + "is string");
                edit.putString(key,(String)value);
            }else if(value instanceof Boolean){
                Util.log(key + "is boolean");
                edit.putBoolean(key, (Boolean) value);
            }else if(value instanceof Integer){
                Util.log(key + "is int");
                edit.putInt(key, (Integer) value);
            }else if(value instanceof Long){
                Util.log(key + "is long");
                edit.putLong(key, (Long) value);
            }
        }
        if(edit.commit()){
            changed = false;
            toSave.clear();
            Snackbar.make(rootLayout,R.string.snack_notify_saved,Snackbar.LENGTH_SHORT).show();
            updateAllMenu();
        }else{
            Snackbar.make(rootLayout,R.string.snack_notify_save_falled,Snackbar.LENGTH_LONG).show();
        }

        updateAllMenu();
    }
    private boolean changedAuto = false;
    private int brightnessMode = -1;
    @Override
    public void setBrightness(int bright) {
        if(brightnessMode == -1){
            try {
                brightnessMode = Settings.System.getInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS_MODE);
            } catch (Settings.SettingNotFoundException e) {
                brightnessMode = -2;
                e.printStackTrace();
            }
        }
        if(!changedAuto){
            if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                Settings.System.putInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS_MODE,
                        Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                changedAuto = true;
            }
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = (float)bright / (float)100;
        getWindow().setAttributes(lp);
    }

    @Override
    public void recoverBright() {
        if(changedAuto){
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
            changedAuto = false;
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = -1f;
        getWindow().setAttributes(lp);
    }

    @Override
    public void setActionBarColor(int back,int title,int status) {
        toolbar.setBackgroundColor(back);
        toolbar.setTitleTextColor(title);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        this.getWindow().setStatusBarColor(status);
    }

    @Override
    public void setActionBarOrigin() {
        toolbar.setBackgroundColor(getColor(R.color.colorPrimary));
        toolbar.setTitleTextColor(Color.WHITE);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        this.getWindow().setStatusBarColor(getColor(android.R.color.transparent));
    }

    public class PagerAdapter extends FragmentPagerAdapter {

        private IFragmentSupply supplier;
        private int[] identifier;

        public PagerAdapter(FragmentManager fm,IFragmentSupply supply) {
            super(fm);
            supplier = supply;
            identifier = supply.getIdentifier();
        }
        private int convPos(int ps){
            return identifier[ps];
        }

        @Override
        public Fragment getItem(int position) {
            return supplier.getItem(convPos(position));
        }

        @Override
        public int getCount() {
            return identifier.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return supplier.getTitle(convPos(position));
        }
    }
}
