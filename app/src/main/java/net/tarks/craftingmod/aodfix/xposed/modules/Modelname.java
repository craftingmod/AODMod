package net.tarks.craftingmod.aodfix.xposed.modules;

import net.tarks.craftingmod.aodfix.xposed.Module;

import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Youtube 60fps Fixer
 */
public class Modelname extends Module {

    public static String[] allowedPackages = new String[]{"com.samsung.android.themestore","com.google.android.youtube","com.sec.android.app.samsungapps"};

    @Override
    public void load() {
        if(packageName.equalsIgnoreCase("com.samsung.android.themestore")
                || packageName.equalsIgnoreCase("com.sec.android.app.samsungapps")){
            XposedHelpers.setStaticObjectField(XposedHelpers.findClass("android.os.Build",loader),"MODEL","SM-N930S");
        }else if(packageName.equalsIgnoreCase("com.google.android.youtube")){
            XposedHelpers.setStaticObjectField(XposedHelpers.findClass("android.os.Build",loader),"MODEL","SM-G900F");
        }
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.samsung.android.themestore")
                || packageName.equalsIgnoreCase("com.sec.android.app.samsungapps")
                || packageName.equalsIgnoreCase("com.google.android.youtube");
    }

    protected Modelname(XC_LoadPackage.LoadPackageParam pm) {
        super(pm);
    }
    public static Modelname newInstance(XC_LoadPackage.LoadPackageParam pm){
        return new Modelname(pm);
    }
}
