package net.tarks.craftingmod.aodfix.xposed.modules;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.os.Handler;

import com.android.craftingmod.AODTouchDaemon;

import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.xposed.Module;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by superuser on 16/11/13.
 */

public class Touch extends Module {
    public static String[] allowedPackages = new String[]{"android"};

    @Override
    public void load() {
        Class<?> PowerManager = XposedHelpers.findClassIfExists("com.android.server.power.PowerManagerService$BinderService",loader);
        Class<?> AODTouch = XposedHelpers.findClassIfExists("com.samsung.android.aod.AODTouchDaemon",loader);
        Class<?> KPPDaemon = XposedHelpers.findClassIfExists("com.samsung.android.bluelightfilter.KPPDaemon",loader);
        if(AODTouch != null && KPPDaemon != null){
            Class<?> DPC = XposedHelpers.findClass("com.android.server.display.DisplayPowerController",loader);
            Class<?> DPCallback = XposedHelpers.findClass("android.hardware.display.DisplayManagerInternal.DisplayPowerCallbacks",loader);
            Class<?> DBlanker = XposedHelpers.findClass("com.android.server.display.DisplayBlanker",loader);
            /*
            XposedHelpers.findAndHookConstructor(DPC, Context.class, DPCallback, android.os.Handler.class, SensorManager.class, DBlanker, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    XposedHelpers.setIntField(param.thisObject,"mScreenBrightnessDozeConfig",-1);
                }
            });
            */
            Util.log("Magma custom detected. battery saver on");
            XposedHelpers.findAndHookMethod(KPPDaemon, "initHandler", Context.class, new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                    Object _this = param.thisObject;
                    Context context = (Context) param.args[0];
                    XposedHelpers.setObjectField(_this,"context",context);
                    XposedHelpers.setObjectField(_this,"handler",new Handler(context.getMainLooper(), (Handler.Callback) _this));
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("tomorrow.blueLight.wake");
                    filter.addAction("tomorrow.aod.touch.lock");
                    context.registerReceiver((BroadcastReceiver) _this,filter);
                    return null;
                }
            });
            XposedHelpers.findAndHookMethod(KPPDaemon, "onReceive", Context.class, Intent.class, new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                    Intent i = (Intent) param.args[1];
                    if(i.getAction().equalsIgnoreCase("tomorrow.aod.touch.lock")){
                        AODTouchDaemon d;
                        if(XposedHelpers.getAdditionalInstanceField(param.thisObject,"mTouch") == null){
                            d = new AODTouchDaemon();
                            XposedHelpers.setAdditionalInstanceField(param.thisObject,"mTouch",d);
                        }else{
                            d = (AODTouchDaemon) XposedHelpers.getAdditionalInstanceField(param.thisObject,"mTouch");
                        }
                        d.blockTouch();
                    }else{
                        Handler h = (Handler) XposedHelpers.getObjectField(param.thisObject,"handler");
                        h.sendEmptyMessage(0);
                    }
                    return null;
                }
            });
        }
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("android");
    }
    protected Touch(XC_LoadPackage.LoadPackageParam pm, XSharedPreferences pref) {
        super(pm,pref);
    }
    public static Touch newInstance(XC_LoadPackage.LoadPackageParam pm,XSharedPreferences pf){
        return new Touch(pm,pf);
    }
}
