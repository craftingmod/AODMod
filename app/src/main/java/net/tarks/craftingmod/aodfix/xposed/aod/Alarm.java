package net.tarks.craftingmod.aodfix.xposed.aod;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

/**
 * Created by superuser on 16/6/12.
 */

public class Alarm {
    private Context context;
    private AlarmManager alarmManager;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private Intent i;
    public Alarm(Context ct, PowerManager wl){
        context = ct;
        powerManager = wl;
        i = new Intent(context, Alarm.class);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"AODScreen");
    }
    public void unwake(){
        if(wakeLock.isHeld()){
            wakeLock.release();
        }
    }
    public void wakeInf(){
        if(wakeLock.isHeld()){
            wakeLock.release();
        }
        wakeLock.acquire();
    }
    public void wake(long clock){
        if(wakeLock.isHeld()){
            wakeLock.release();
        }
        wakeLock.acquire(1000*clock);
    }
}
