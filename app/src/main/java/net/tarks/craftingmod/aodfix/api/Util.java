package net.tarks.craftingmod.aodfix.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import net.tarks.craftingmod.aodfix.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import io.feeeei.circleseekbar.CircleSeekBar;

/**
 * Created by superuser on 16/6/18.
 */

public class Util {
    public static final String CONFIG_SLEEP = "autoSleep";
    public static final String CONFIG_AUTO_LIGHT = "autoBrightness";
    public static final String CONFIG_BRIGHTNESS = "brightness";
    public static final String CONFIG_DELAY_WIFI = "onWifi";
    public static final String CONFIG_DELAY_MOBILE = "onData";
    public static final String CONFIG_DEBUG = "debug";

    private Util(){}

    public static boolean debug = false;
    public static boolean useLog = false;

    public static String getCalledName(){
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String classname = null;
        //String thisclass = Util.class.getCanonicalName();
        for(int i=0;i<stackTraceElements.length;i+=1){
            classname = stackTraceElements[i].getClassName();
            if(classname.equals("dalvik.system.VMStack") || classname.equals("java.lang.Thread") ||
                    classname.equals(Util.class.getCanonicalName()) /* || classname.equals(thisclass) */){
                classname = null;
                continue;
            }else{
                classname = classname.substring(Math.max(0,classname.lastIndexOf("."))) + "#" + stackTraceElements[i].getMethodName();
                break;
            }
        }
        if(classname == null){
            return "null";
        }else{
            return classname;
        }
    }
    public static long getDeepSleep(){
        return SystemClock.elapsedRealtime() - SystemClock.uptimeMillis();
    }
    public static XSharedPreferences getConfig(){
        return new XSharedPreferences("net.tarks.craftingmod.aodfix","config");
        //return new XSharedPreferences("net.tarks.craftingmod.aodfix","config");
    }
    @SuppressWarnings("deprecation")
    @SuppressLint("WorldReadableFiles")
    public static SharedPreferences getConfig(Context context){
        return context.getSharedPreferences("config",Context.MODE_WORLD_READABLE);
    }
    public static void log(String msg){
        if(!useLog){
            return;
        }
        StringBuilder b = new StringBuilder();
        b.append("AODFix ");
        b.append(Util.getCalledName());
        b.append(" - ");
        b.append(getDate());
        b.append("\n > ");
        b.append(msg);
        try{
            XposedBridge.log(b.toString());
        }catch (java.lang.NoClassDefFoundError | Exception e){
            Log.d("AODMod",b.toString());
        }
    }
    public static String getDate(){
        SimpleDateFormat formatter = new SimpleDateFormat( "h:mm:ss:SSSS", Locale.KOREA );
        Date currentTime = new Date();
        return formatter.format(currentTime);
    }
    public static void fixCircleSeekbar(final CircleSeekBar bar,final int SizeInPixel){
        ViewTreeObserver vto = bar.getViewTreeObserver();

        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private boolean updated = false;
            @Override
            public void onGlobalLayout() {
                if(!updated){
                    // put updated boolean for only once draw
                    updated = true;

                    // getWidth said wrong number. so, should input number.
                    int pxCircle = SizeInPixel;
                    int measureInt = View.MeasureSpec.makeMeasureSpec(pxCircle, View.MeasureSpec.EXACTLY);
                    //outerBar.setLayoutParams(params);
                    bar.measure(measureInt, measureInt);
                    // outerBar.refershPosition()
                    // cause must correct at first view
                    try {
                        // refersh? refresh is correct but :p
                        Method refresh = bar.getClass().getDeclaredMethod("refershPosition");
                        refresh.setAccessible(true);
                        refresh.invoke(bar);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    // redraw for correct position.
                    bar.invalidate();
                }
            }
        });
    }
    public static String parseDuration(SharedPreferences pref,Context con,String code){
        int total = Math.max(Math.min(86399,pref.getInt(code,900)),1);
        int hour = (int) Math.floor(total/3600);
        int minutes = (int) Math.floor((total%3600)/60);
        boolean infinity = pref.getBoolean(code + "_infinity",true);

        String timeFormat_H = con.getString(R.string.timeFormat_H);
        String timeFormat_M = con.getString(R.string.timeFormat_M);
        String hourStr = hour + "";
        String minuteStr = minutes > 9 ? minutes + "" : "0" + minutes;
        String result;
        if(infinity){
            result = con.getString(R.string.time_duration_infinity);
        }else if(hour == 0){
            result = timeFormat_M.replace("%p",minutes+"");
        }else if(minutes == 0){
            result = timeFormat_H.replace("%p",hourStr);
        }else{
            result = timeFormat_H.replace("%p",hourStr) + " " + timeFormat_M.replace("%p",minuteStr);
        }
        return result;
    }
    public static String parseDuration(Context con,String code){
        SharedPreferences pref = getConfig(con);
        return parseDuration(pref,con,code);
    }
    public static String parseBrightness(SharedPreferences pref,Context con){
        boolean auto = pref.getBoolean(Constants.PREF_AOD_BRIGHTNESS_AUTO,false);
        if(auto){
            return con.getString(R.string.bright_text_auto);
        }else{
            return pref.getInt(Constants.PREF_AOD_BRIGHTNESS_VALUE,20) + "%";
        }
    }
}
