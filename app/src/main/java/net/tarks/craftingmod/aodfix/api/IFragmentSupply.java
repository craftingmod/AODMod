package net.tarks.craftingmod.aodfix.api;

import android.support.v4.app.Fragment;

/**
 * Created by jhrunning on 01/10/2016.
 */

public interface IFragmentSupply {
    Fragment getItem(int identify);
    int[] getIdentifier();
    String getTitle(int identify);
}
