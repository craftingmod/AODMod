package net.tarks.craftingmod.aodfix.api;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import net.tarks.craftingmod.aodfix.R;

/**
 * Created by jhrunning on 01/10/2016.
 */

public class NonSwipeableViewPager extends ViewPager {
    private boolean swipeable;

    public NonSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NonSwipeableViewPager);
        try {
            swipeable = a.getBoolean(R.styleable.NonSwipeableViewPager_swipeable, true);
        } finally {
            a.recycle();
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return swipeable && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return swipeable && super.onTouchEvent(event);
    }
}
