package net.tarks.craftingmod.aodfix.xposed.modules;

import android.content.ContentResolver;
import android.content.Intent;

import net.tarks.craftingmod.aodfix.xposed.Module;

import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by superuser on 16/9/11.
 */

public class Mtp extends Module {
    public static String[] allowedPackages = new String[]{"com.samsung.android.MtpApplication"};

    protected Mtp(XC_LoadPackage.LoadPackageParam pm) {
        super(pm);
    }

    @Override
    public void load() {
        Class<?> usb = XposedHelpers.findClass("com.samsung.android.MtpApplication.USBConnection",loader);
        final Class<?> receiver = XposedHelpers.findClass("com.samsung.android.MtpApplication.MtpReceiver",loader);
        final Class<?> system = XposedHelpers.findClass("android.provider.Settings.System",loader);
        final Class<?> secure = XposedHelpers.findClass("android.provider.Settings.Secure",loader);
        XposedHelpers.findAndHookMethod(usb, "showDiaglog", new XC_MethodReplacement() {
            @Override
            protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                Object _this = param.thisObject;
                Object mtpReceiver = XposedHelpers.newInstance(receiver);
                XposedHelpers.setObjectField(_this,"mReceiver",mtpReceiver);
                Intent intent = (Intent) XposedHelpers.callMethod(_this,"getIntent");
                if(intent.getExtras() == null){
                    XposedHelpers.callMethod(_this,"finish");
                    return null;
                }
                String modeType = intent.getExtras().getString("mode");
                if(modeType == null){
                    XposedHelpers.callMethod(_this,"finish");
                    return null;
                }
                try{
                    ContentResolver cr = (ContentResolver) XposedHelpers.callMethod(
                            XposedHelpers.getStaticObjectField(receiver,"mContext"),"getContentResolver");
                    if(cr == null){
                        XposedHelpers.callMethod(_this,"finish");
                        return null;
                    }else if(!(XposedHelpers.callStaticMethod(system,"getInt",
                            new Class[]{ContentResolver.class,String.class,int.class},
                            cr,"mtp_running_status",0).equals(1))){
                        XposedHelpers.callMethod(_this,"finish");
                        //System.getInt(cr, "mtp_running_status", 0) != 1
                        return null;
                    }else{
                        XposedHelpers.callMethod(XposedHelpers.getObjectField(_this,"mReceiver"),
                                "changeMtpMode");
                        XposedHelpers.callMethod(_this,"finish");
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    XposedHelpers.callMethod(_this,"finish");
                }
                return null;
            }
        });
    }

    @Override
    public boolean filter() {
        return packageName.equalsIgnoreCase("com.samsung.android.MtpApplication");
    }
    public static Mtp newInstance(XC_LoadPackage.LoadPackageParam pm){
        return new Mtp(pm);
    }

}
