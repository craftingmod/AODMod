package net.tarks.craftingmod.aodfix.ui;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.ui.base.BaseFragment;

/**
 * Created by superuser on 16/10/3.
 */

public class InfoFragment extends BaseFragment {
    private View view;
    public InfoFragment(){}
    public static InfoFragment newInstance(){
        return new InfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.info_frag, container, false);
        TextView textAOD = (TextView) view.findViewById(R.id.aodtext);
        String version;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo("com.samsung.android.app.aodservice", 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "안깔림";
        }
        textAOD.setText(context.getString(R.string.aodver).replace("%p",version));
        return view;
    }
}
