package net.tarks.craftingmod.aodfix.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;

import net.tarks.craftingmod.aodfix.R;
import net.tarks.craftingmod.aodfix.api.Constants;
import net.tarks.craftingmod.aodfix.api.Util;
import net.tarks.craftingmod.aodfix.ui.base.DarkFragment;

/**
 * Created by superuser on 16/10/3.
 */

public class AODNotiFragment extends DarkFragment {
    private View view;
    private SwitchButton switchNum;
    private SwitchButton switchColor;
    private SwitchButton switchGreen;
    private SharedPreferences pref;

    private boolean useColor;
    private boolean useNumber;
    private boolean useGreen;

    private View colorCircle;
    private View blackCircle;
    private TextView numberText;

    public AODNotiFragment(){}
    public static AODNotiFragment getInstance(){
        return new AODNotiFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.aodnoti_frag, container, false);
        pref = Util.getConfig(context);
        useColor = pref.getBoolean(Constants.PREF_AOD_NOTIFICATION_COLOR,false);
        useNumber = pref.getBoolean(Constants.PREF_AOD_NOTIFICATION_NUMBER, true);
        useGreen = pref.getBoolean(Constants.PREF_AOD_BATTERY_SAFETY, false);

        initCreateView();
        colorCircle = view.findViewById(R.id.colorCircle);
        blackCircle = view.findViewById(R.id.blackCircle);
        numberText = (TextView) view.findViewById(R.id.textNum);
        numberText.setText(Integer.toString((int)Math.floor(Math.random()*1000)));

        switchNum = (SwitchButton) view.findViewById(R.id.switch_number);
        switchColor = (SwitchButton) view.findViewById(R.id.switch_color);
        switchGreen = (SwitchButton) view.findViewById(R.id.switch_battery);
        switchNum.setChecked(useNumber);
        switchColor.setChecked(useColor);
        switchGreen.setChecked(useGreen);
        switchNum.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b != useNumber){
                useNumber = b;
                updateStatus(true);
            }
        });
        switchColor.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b != useColor){
                useColor = b;
                updateStatus(true);
            }
        });
        switchGreen.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b != useGreen){
                useGreen = b;
                updateStatus(true);
            }
        });
        updateStatus(false);
        return view;
    }
    private void updateStatus(boolean save){
        colorCircle.setVisibility(useColor?View.VISIBLE:View.GONE);
        blackCircle.setVisibility(useColor?View.GONE:View.VISIBLE);
        numberText.setVisibility(useNumber?View.VISIBLE:View.GONE);
        if(save){
            mSaver.request(Constants.PREF_AOD_NOTIFICATION_NUMBER,useNumber);
            mSaver.request(Constants.PREF_AOD_NOTIFICATION_COLOR,useColor);
            mSaver.request(Constants.PREF_AOD_BATTERY_SAFETY,useGreen);
            mSaver.updateSave();
        }
    }
}
