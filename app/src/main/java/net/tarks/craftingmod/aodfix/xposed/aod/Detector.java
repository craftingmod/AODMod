package net.tarks.craftingmod.aodfix.xposed.aod;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import net.tarks.craftingmod.aodfix.api.Util;

import de.robv.android.xposed.XposedHelpers;

/**
 * Deep sleep detector
 * Created by superuser on 16/6/18.
 */

public class Detector implements Runnable {
    private long deepSleep;
    private final int delay = 1000;
    private final Object AODService;
    private Handler handler;
    public Detector(Object service){
        AODService = service;
    }
    public void reset(Handler h){
        handler = h;
        deepSleep = -1;
    }
    public void destroy(){
        try{
            handler.removeCallbacks(this);
        }catch (Exception e){}
    }
    @Override
    public void run() {
        long nowSlp = Util.getDeepSleep();
        if(deepSleep < 0){
            deepSleep = nowSlp;
        }
        if(nowSlp - deepSleep > 20){
            Util.log("destroy");
            destroy();
            exit();
        }else{
            handler.postDelayed(this,delay);
        }
    }

    private void exit(){
        Util.log("Closing AOD..");
        Context c = (Context) AODService;
        Intent i = new Intent();
        i.setAction("tomorrow.aod.touch.lock");
        c.sendBroadcast(i);

        Handler h = (Handler) XposedHelpers.getObjectField(AODService,"mUIHandler");
        h.sendEmptyMessage(XposedHelpers.getIntField(AODService,"UIHANDLER_FORCE_STOP_ALPM"));
        handler.post(new Runnable() {
            @Override
            public void run() {
                XposedHelpers.callMethod(AODService,"aodScreenTurnedOn");
            }
        });
    }
}
