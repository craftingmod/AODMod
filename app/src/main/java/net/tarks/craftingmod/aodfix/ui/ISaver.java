package net.tarks.craftingmod.aodfix.ui;

/**
 * Created by superuser on 16/10/2.
 */

public interface ISaver {
    void request(String id,Object value);
    void requestTitleChange(int id,String title);
    void updateSave();
    void flush();
}
