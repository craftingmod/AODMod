package net.tarks.craftingmod.aodfix.ui.base;

import net.tarks.craftingmod.aodfix.R;

/**
 * Created by superuser on 16/10/3.
 */

public abstract class DarkFragment extends BaseFragment {

    protected boolean requireInit = false;

    public void initCreateView(){
        if(requireInit){
            requireInit = false;
            onFirstInit();
        }
    }
    protected void switchActionbar(boolean dark){
        if(dark){
            int colorBack = context.getColor(R.color.dark_actionbar_back);
            int colorText = context.getColor(R.color.dark_actionbar_text);
            mBrightCon.setActionBarColor(colorBack,colorText,colorBack);
        }else{
            mBrightCon.setActionBarOrigin();
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(context != null){
            onSwitchedFragment(isVisibleToUser);
        }else{
            if(!requireInit && isVisibleToUser){
                requireInit = true;
            }
        }
    }
    protected void onFirstInit(){
        switchActionbar(true);
    }
    protected void onSwitchedFragment(boolean visible){
        switchActionbar(visible);
    }
}
